import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import { createStore, applyMiddleware, compose, combineReducers } from "redux";
import thunk from "redux-thunk";
import "typeface-roboto";

import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import authReducer from "./store/reducers/auth";
import userReducer from "./store/reducers/user";
import restaurantReduser from "./store/reducers/restaurant";
import productReduser from "./store/reducers/product";
import productsReduser from "./store/reducers/products";
import cartReduser from "./store/reducers/cart";
import orderReduser from "./store/reducers/order";
import storeReduser from "./store/reducers/store";
import favoriteReduser from "./store/reducers/favorite";
import favoritesReduser from "./store/reducers/favorites";
import { Provider } from "react-redux";

// declare global {
//     interface Window { __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any; }
// }

// window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || {};
const composeEnhancers =
  process.env.NODE_ENV === "development"
    ? (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    : null || compose;
// const composeEnhancers = compose;

const rootReducer = combineReducers({
  auth: authReducer,
  user: userReducer,
  restaurant: restaurantReduser,
  product: productReduser,
  products: productsReduser,
  cart: cartReduser,
  order: orderReduser,
  store: storeReduser,
  favorite: favoriteReduser,
  favorites: favoritesReduser,
});

const store = createStore(
  rootReducer,
  applyMiddleware(thunk)
  //   composeEnhancers(applyMiddleware(thunk))
);

const app = (
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>
);

ReactDOM.render(app, document.getElementById("root"));
serviceWorker.register();

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();
