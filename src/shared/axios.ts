import axios from 'axios';

const instance = axios.create({
    baseURL: 'http://localhost:62296/'
});

instance.interceptors.request.use(config => {
    let token = localStorage.getItem('token');

    if (token) {
        config.headers['Authorization'] = ' Bearer ' + token;
    }
    
    return config;
});

export default instance;
