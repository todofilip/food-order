import React from "react";
import { Button } from "@material-ui/core";
import ImageUploading from "react-images-uploading";

interface UploadImageProps {
  image: any;
  onImageChange(imageList: any, addUpdateIndex: any): void;
}

const UploadImage: React.SFC<UploadImageProps> = (props) => {
  const { onImageChange } = props;
  const { image } = props;

  return (
    <div style={{ marginTop: 20, marginBottom: 20 }}>
      <ImageUploading value={[]} onChange={onImageChange} dataURLKey="data_url">
        {({ imageList, onImageUpdate, onImageRemove }) => (
          <div className="upload__image-wrapper">
            <div className="image-item">
              <img src={image} alt="" width="300" />
              <div
                className="image-item__btn-wrapper"
                style={{ marginTop: 10 }}
              >
                <Button
                  variant="contained"
                  className="pull-left"
                  style={{
                    backgroundColor: "limegreen",
                  }}
                  onClick={() => onImageUpdate(0)}
                >
                  Upload image
                </Button>
                <Button
                  variant="contained"
                  style={{
                    backgroundColor: "yellow",
                    marginLeft: 20,
                  }}
                  onClick={() => onImageRemove(0)}
                >
                  Remove image
                </Button>
              </div>
            </div>
          </div>
        )}
      </ImageUploading>
    </div>
  );
};

export default UploadImage;
