import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import IconButton from "@material-ui/core/IconButton";
import AddCircleOutlineIcon from "@material-ui/icons/AddCircleOutline";
import RemoveCircleOutlineIcon from "@material-ui/icons/RemoveCircleOutline";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { makeStyles, Theme, createStyles } from "@material-ui/core";
import { connect } from "react-redux";


const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    price: {
      fontWeight: "bold",
    },
    input: {
      width: 50,
      textAlign: "right",
    },
  })
);

interface YesNoModalProps {
  question: any;
  open: boolean;
  onClose(): void;
  onAccept(product: any): void;
}

const YesNoModal: React.SFC<YesNoModalProps> = (props) => {
  const [state, setState] = useState({
    quantity: 1,
  });

  const classes = useStyles();
  const { onClose, onAccept, question } = props;

  return (
    <div>
      <Dialog
        open={props.open}
        onClose={props.onClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogContent>
          <DialogContentText>
            {question}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={onClose}
            color="primary"
            variant="contained"
          >
            No
          </Button>
          <Button
            onClick={onAccept}
            color="primary"
            variant="contained"
          >
            Yes
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default YesNoModal;
