import React, { useEffect } from "react";
import { connect } from "react-redux";
import { makeStyles, createStyles, Theme } from "@material-ui/core";

import * as actions from "../../store/actions/index";
import Spinner from "../UI/Spinner/Spinner";
import Order from "./Order/Order";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      backgroundColor: theme.palette.background.paper,
    },
  })
);

interface OrdersProps {
  orders: any;
  error: any;
  loading: boolean;
  requestSent: boolean;
  restaurants: any;
  userId: string;
  token: string;
  onFetchOrders(userId: string, token: string): void;
  onClearOrders(): void;
}

const Orders: React.SFC<OrdersProps> = (props) => {
  const classes = useStyles();
  const { orders } = props;
  const { restaurants } = props;
  const { requestSent } = props;
  const { onFetchOrders } = props;
  const { onClearOrders } = props;

  useEffect(() => {
    if (!requestSent && !orders) {
      onFetchOrders(props.userId, props.token);
    }
  }, [requestSent, orders, props.userId, props.token]);

  useEffect(() => {
    return () => {
      onClearOrders();
    };
  }, []);

  let content;

  if (orders) {
    content = orders.map((order: any) => {
      return <Order key={order.id} order={order} restaurants={restaurants} />;
    });
  } else {
    content = <Spinner />;
  }

  return <div className={classes.root}>{content}</div>;
};

const mapStateToProps = (state: any) => {
  return {
    orders: state.order.orders,
    error: state.order.error,
    loading: state.order.loading,
    requestSent: state.order.requestSent,
    restaurants: state.restaurant.restaurants,
    userId: state.auth.userId,
    token: state.auth.token,
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    onFetchOrders: (userId: string, token: string) =>
      dispatch(actions.fetchOrders(userId, token)),
    onClearOrders: () => dispatch(actions.clearOrders()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Orders);
