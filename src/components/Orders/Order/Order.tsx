import React from "react";
import { useHistory } from "react-router-dom";
import {
  ListItem,
  ListItemText,
  Divider,
  Typography,
  makeStyles,
  Theme,
  createStyles,
  Button,
} from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: "100%",
      maxWidth: "36ch",
      backgroundColor: theme.palette.background.paper,
    },
    inline: {
      display: "inline",
    },
    minOrder: {
      display: "inline",
      fontSize: 14,
    },
  })
);

interface OrderProps {
  order: any;
  restaurants: any;
}

const Order: React.SFC<OrderProps> = (props) => {
  const classes = useStyles();
  const history = useHistory();
  const { order } = props;

  const getTotal = (items: any) => {
    return items.reduce(function (a: any, b: any) {
      return a + b.price * b.quantity;
    }, 0);
  };

  const formatDate = (dateString: any) => {
    let date = new Date(dateString);
    let dd = String(date.getDate()).padStart(2, "0");
    let mm = String(date.getMonth() + 1).padStart(2, "0");
    let yyyy = date.getFullYear();
    let hours = date.getHours();
    let minutes = date.getMinutes();
    return mm + "." + dd + "." + yyyy + " at " + hours + ":" + minutes;
  };

  const onOrderClick = () => {
    history.push("restaurant/" + order.storeId + "?" + order.id);
  };

  const button = (
    <Button variant="contained" color="primary" onClick={onOrderClick}>
      Edit
    </Button>
  );

  return (
    <React.Fragment>
      <ListItem>
        <ListItemText
          primary={
            <Typography
              component="span"
              variant="body2"
              className={classes.inline}
              color="textPrimary"
            >
              {order ? "Total: " + getTotal(order.orderLines) + " RSD" : null}
            </Typography>
          }
          secondary={
            <React.Fragment>
              <Typography
                component="span"
                variant="body2"
                className={classes.inline}
                color="textSecondary"
              >
                {order && order.storeName ? order.storeName : ""}
              </Typography>
              <br />
              <Typography
                component="span"
                variant="body2"
                className={classes.minOrder}
                color="textSecondary"
              >
                {order && order.date ? formatDate(order.date) : ""}
              </Typography>
              <br />
              <Typography
                component="span"
                variant="body2"
                className={classes.minOrder}
                style={
                  order && order.status && order.status === "delivered"
                    ? { color: "limegreen" }
                    : { color: "orange" }
                }
              >
                {order && order.status ? "Status: " + order.status : ""}
              </Typography>
            </React.Fragment>
          }
        />
        {order && order.status === "ordered" ? button : null}
      </ListItem>
      <Divider variant="fullWidth" />
    </React.Fragment>
  );
};

export default Order;
