import React, { useState } from "react";
import { Grid, Paper } from "@material-ui/core";
import "./Help.css";

interface HelpProps {}

const Help: React.SFC<HelpProps> = (props) => {
  return (
    <Grid>
      <h1 className="header">Help & FAQ</h1>
      <h2 className="header">How do I place an order?</h2>
      <div className="help-div">
        <p className="help-p">
          Ordering your order is not so complicate. You just need to follow
          these steps:
          <br />
          (1) Go to Home screen (click on logo)
          <br />
          (2) Now list of restaurants are shown. Choose the restaurant you want,
          and click Order
          <br />
          (3) Now list of products are shown. Choose the product you want,
          clicking on it
          <br />
          (4) Modal window is shown, with mora details about the selected
          product. You can now choose the amount by clicking on '+' and '-'
          button, then click ADD TO CART button to add product in your cart
          <br />
          (5) On the right side, you can now see your product is added in cart
          with desired amount. You can now repeat the process if you want more
          products to add in cart
          <br />
          (6) When you finish adding desired products in cart, and want to
          finish your order, you just need to click ORDER button in the bottom
          of your cart
          <br />
          (7) Now you will see the screen with message "Your order has been sent
          successfuly!" if your order is created
        </p>
      </div>
      <h2 className="header">How do I add restaurant into my favorites?</h2>
      <div className="help-div">
        <p className="help-p">
          When you successfuly finish your order, "Add store to favorites" will
          be shown to you in case that restaurant is not already in your
          favorites list. Clicking on that button, you will add that restaurant
          into your favorites list
        </p>
      </div>
      <h2 className="header">How do I remove restaurant from my favorites?</h2>
      <div className="help-div">
        <p className="help-p">
          (1) From upper right corner, click on the hamburger menu icon, then
          select 'My favorites'
          <br />
          (2) Your list of favorites are shown. Choose restaurant that you want
          to remove from your favorites, then click on heart icon to remove. Now
          you can see how current restaurant dissapeared from your favorites
          list
        </p>
      </div>
      <h2 className="header">How do I edit my order?</h2>
      <div className="help-div">
        <p className="help-p">
          (1) From upper right corner, click on the hamburger menu icon, then
          select 'My orders'
          <br />
          (2) Your list of orders are shown. Only orders which do not have
          'delivered' status can be edited
          <br />
          (3) Choose order that you want to edit, then click on Edit button
          <br />
          (4) Your order cart will be shown, so you can edit products and
          amounts, after which you need to confirm with Order button
        </p>
      </div>
      <h2 className="header">How do I edit my user info?</h2>
      <div className="help-div">
        <p className="help-p">
          (1) From upper right corner, click on the hamburger menu icon, then
          select 'My orders'
          <br />
          (2) Now you can see your user info details, which you can edit field
          by field, and then click on Update button
        </p>
      </div>
    </Grid>
  );
};

export default Help;
