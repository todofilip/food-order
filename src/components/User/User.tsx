import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Link, Redirect } from "react-router-dom";
import {
  Grid,
  Paper,
  Button,
  Switch,
  FormControlLabel,
} from "@material-ui/core";
import FormGroup from "@material-ui/core/FormGroup";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";

import Spinner from "../UI/Spinner/Spinner";
import * as actions from "../../store/actions/index";
import { updateObject } from "../../shared/utility";

interface UserProps {
  user: any;
  userId: string;
  token: string;
  error: any;
  loading: boolean;
  isAuthenticated: boolean;
  requestSent: boolean;
  onFetchUser(userId: string): void;
  onUpdateUser(user: any): void;
}

const User: React.SFC<UserProps> = (props) => {
  const [state, setState] = useState({
    user: {
      username: "",
      firstName: "",
      lastName: "",
      useDarkTheme: false,
      userId: null,
    },
  });

  const { user } = props;
  const { userId } = props;
  const { requestSent } = props;
  const { loading } = props;
  const { onFetchUser } = props;
  const { onUpdateUser } = props;

  useEffect(() => {
    if (!requestSent && !props.user) {
      onFetchUser(userId);
    }

    if (user && !loading && !requestSent) {
      setState(updateObject(state, { user: user }));
    }
  }, [onFetchUser, requestSent, user]);

  const submitHandler = (event: any) => {
    event.preventDefault();
    onUpdateUser(state.user);
  };

  const handleChange = (event: any) => {
    const { user } = state;
    (user as any)[event.target.name] = event.target.value;
    setState({ user });
  };

  const handleSwitchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { user } = state;
    (user as any)[event.target.name] = event.target.checked;
    setState({ user });
  };

  let form;
  if (state.user) {
    form = (
      <div>
        <Grid item xs={12}>
          <TextValidator
            className="textfield"
            value={state.user.username}
            validators={["required"]}
            onChange={handleChange}
            errorMessages={["field is required"]}
            id="username"
            label="Username"
            name="username"
            disabled
          />
          <TextValidator
            className="textfield"
            value={state.user.firstName}
            validators={["required"]}
            onChange={handleChange}
            errorMessages={["field is required"]}
            id="firstName"
            label="First name"
            name="firstName"
          />
          <TextValidator
            className="textfield"
            value={state.user.lastName}
            validators={["required"]}
            onChange={handleChange}
            errorMessages={["field is required"]}
            id="lastName"
            label="Last name"
            name="lastName"
          />
          <FormGroup row>
            <FormControlLabel
              control={
                <Switch
                  checked={state.user.useDarkTheme}
                  onChange={handleSwitchChange}
                  name="useDarkTheme"
                  color="primary"
                />
              }
              label={
                <div>
                  Use dark theme
                  <br />
                  (need relog to apply changes)
                </div>
              }
            />
          </FormGroup>
        </Grid>
        <Grid item className="right top-padding">
          <Link to="/logout">
            <Button color="primary" className="pull-left">
              Logout
            </Button>
          </Link>
          <Button type="submit" variant="contained" color="primary">
            Update
          </Button>
        </Grid>
      </div>
    );
  } else {
    form = <Spinner />;
  }

  let errorMessage = null;

  if (props.error) {
    errorMessage = <p>{props.error.message}</p>;
  }

  let authRedirect = null;

  if (props.error != null || !props.isAuthenticated) {
    authRedirect = <Redirect to="/" />;
  }

  return (
    <Grid>
      <h1 className="header">My profile</h1>
      <Paper className="login">
        {errorMessage}
        <Grid>
          {authRedirect}
          <ValidatorForm
            onSubmit={submitHandler}
            onError={(errors) => console.log(errors)}
          >
            {form}
          </ValidatorForm>
        </Grid>
      </Paper>
    </Grid>
  );
};

const mapStateToProps = (state: any) => {
  return {
    userId: state.auth.userId,
    token: state.auth.token,
    user: state.user.user,
    loading: state.user.loading,
    error: state.user.error,
    requestSent: state.user.requestSent,
    isAuthenticated: state.auth.token !== null,
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    onUpdateUser: (user: any) => dispatch(actions.updateUser(user)),
    onFetchUser: (userId: string) => dispatch(actions.fetchUser(userId)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(User);
