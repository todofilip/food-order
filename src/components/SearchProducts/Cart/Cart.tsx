import React, { useState, useEffect } from "react";
import {
  makeStyles,
  Theme,
  createStyles,
  Box,
  Card,
  Button,
} from "@material-ui/core";
import { connect } from "react-redux";
import * as actions from "../../../store/actions/index";
import CartItems from "./CartItems/CartItems";
import { Redirect } from "react-router-dom";
import Spinner from "../../UI/Spinner/Spinner";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      // display: "flex",
      width: 400,
      padding: 10,
      textAlign: "center",
    },
    cartHeader: {
      textAlign: "center",
    },
    orderButton: {
      margin: 10,
    },
    cartTotal: {
      textAlign: "left",
      paddingLeft: 16,
    },
  })
);

interface CartProps {
  productsInCart: any | null;
  restaurantId: string;
  userId: string;
  token: string;
  loading: boolean;
  error: any | null;
  posted: boolean;
  onPostOrder(
    products: any,
    restaurantId: string,
    userId: string
  ): void;
}

const Cart: React.SFC<CartProps> = (props) => {
  const classes = useStyles();
  const { productsInCart } = props;
  const { restaurantId } = props;
  const { userId } = props;
  const { onPostOrder } = props;

  const getTotal = (items: any) => {
    return items.reduce(function (a: any, b: any) {
      return a + b.price * b.quantity;
    }, 0);
  };

  const onOrderClick = () => {
    onPostOrder(productsInCart, restaurantId, userId);
  };

  let successRedirect = null;

  if (props.posted) {
    successRedirect = <Redirect to={'/success/' + restaurantId}  />;
  }

  return (
    <div className={classes.root}>
      {successRedirect}
      {props.loading ? (
        <Spinner />
      ) : (
        <Card>
          <p className={classes.cartHeader}>Your cart</p>
          {productsInCart.length != 0 ? (
            <CartItems products={productsInCart} />
          ) : (
            <p className={classes.cartHeader}>Empty</p>
          )}
          {productsInCart.length != 0 ? (
            <React.Fragment>
              <p className={classes.cartTotal}>
                <b>
                  Total:{" "}
                  {productsInCart.length > 0
                    ? getTotal(productsInCart).toFixed(2) + " RSD"
                    : null}
                </b>
              </p>
              <Button
                color="primary"
                variant="contained"
                className={classes.orderButton}
                onClick={onOrderClick}
              >
                Order
              </Button>
            </React.Fragment>
          ) : null}
        </Card>
      )}
    </div>
  );
};

const mapStateToProps = (state: any) => {
  return {
    productsInCart: state.cart.productsInCart,
    loading: state.cart.loading,
    error: state.cart.error,
    posted: state.cart.posted,
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    onPostOrder: (
      products: any,
      restaurantId: string,
      userId: string
    ) => dispatch(actions.postOrder(products, restaurantId, userId)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
