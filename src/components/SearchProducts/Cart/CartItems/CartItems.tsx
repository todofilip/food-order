import React from "react";
import { makeStyles, Theme, createStyles, List } from "@material-ui/core";

import CartItem from "./CartItem/CartItem";

interface CartItemsProps {
  products: any;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      maxWidth: 400,
    },
    cartHeader: {
      textAlign: "center",
    },
  })
);

const CartItems: React.SFC<CartItemsProps> = (props) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <List>
        {(props.products ? props.products : null).map((product: any) => {
          return <CartItem key={product.productId} product={product} />;
        })}
      </List>
    </div>
  );
};

export default CartItems;
