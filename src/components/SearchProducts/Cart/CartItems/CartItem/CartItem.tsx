import React from "react";
import { connect } from "react-redux";
import {
  makeStyles,
  Theme,
  createStyles,
  Divider,
  ListItem,
  ListItemText,
  Typography,
  IconButton,
  ListItemSecondaryAction,
} from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";

import * as actions from "../../../../../store/actions/index";

interface CartItemProps {
  product: any;
  onRemoveProductsFromCart(id: string): void;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: 500,
    },
    cartHeader: {
      textAlign: "center",
    },
    listItem: {
      width: 400,
    },
  })
);

const CartItem: React.SFC<CartItemProps> = (props) => {
  const classes = useStyles();
  const { product } = props;
  const { onRemoveProductsFromCart } = props;

  return (
    <React.Fragment>
      <Divider variant="fullWidth" />
      <ListItem className={classes.listItem}>
        <ListItemText
          primary={product.productName}
          secondary={
            <React.Fragment>
              <Typography
                component="span"
                variant="body2"
                color="textSecondary"
              >
                {product && product.quantity ? "x " + product.quantity : ""}
              </Typography>
              <br />
              <Typography
                component="span"
                variant="body2"
                color="textSecondary"
              >
                {product && product.price
                  ? (product.quantity * product.price).toFixed(2) + " RSD"
                  : ""}
              </Typography>
            </React.Fragment>
          }
        />
        <ListItemSecondaryAction>
          <IconButton
            color="primary"
            edge="end"
            onClick={() => onRemoveProductsFromCart(product.productId)}
          >
            <CloseIcon />
          </IconButton>
        </ListItemSecondaryAction>
      </ListItem>
    </React.Fragment>
  );
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    onAddProductToCart: (product: any) =>
      dispatch(actions.addProductToCart(product)),
    onRemoveProductFromCart: (id: string) =>
      dispatch(actions.removeProductFromCart(id)),
    onRemoveProductsFromCart: (id: string) =>
      dispatch(actions.removeProductsFromCart(id)),
  };
};

export default connect(null, mapDispatchToProps)(CartItem);
