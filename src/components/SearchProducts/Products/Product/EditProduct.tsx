import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { useHistory } from "react-router-dom";
import { Redirect } from "react-router-dom";
import { Grid, Paper, Button } from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";

import Spinner from "../../../UI/Spinner/Spinner";
import * as actions from "../../../../store/actions/index";
import { updateObject } from "../../../../shared/utility";
import UploadImage from "../../../UI/UploadImage/UploadImage";
import YesNoModal from "../../../UI/YesNoModal/YesNoModal";

interface EditProductProps {
  product: any;
  store: any;
  loading: boolean;
  error: any;
  requestSent: boolean;
  deletedSuccessful: boolean;
  isAuthenticated: boolean;
  authRedirectPath: string;
  onUpdateProduct(product: any): void;
  onFetchProduct(productId: string): void;
  onDeleteProduct(productId: string): void;
  onClearProduct(): void;
}

const EditProduct: React.SFC<EditProductProps> = (props) => {
  const [state, setState] = useState({
    product: {
      name: "",
      description: "",
      price: 0,
      storeId: props.store ? props.store.id : null,
      image: "",
    },
    redirectUrl: "",
    isModalOpen: false,
  });

  const history = useHistory();
  const { onUpdateProduct } = props;
  const { onFetchProduct } = props;
  const { onDeleteProduct } = props;
  const { onClearProduct } = props;
  const { product } = props;
  const { requestSent } = props;
  const { loading } = props;
  const { deletedSuccessful } = props;

  useEffect(() => {
    const id = (props as any).match.params.id;

    if (deletedSuccessful) {
      history.push(state.redirectUrl);
      return;
    }

    if (!requestSent && !props.product) {
      onFetchProduct(id);
    }

    if (props.product && !loading && !requestSent) {
      setState(updateObject(state, { product: props.product }));
    }
  }, [onFetchProduct, requestSent, product]);

  useEffect(() => {
    ValidatorForm.addValidationRule("isGreaterThanZero", (value) => value > 0);

    return () => {
      ValidatorForm.removeValidationRule("isGreaterThanZero");
      onClearProduct();
    };
  }, []);

  const handleModalOpen = () => {
    setState({
      ...state,
      isModalOpen: true,
    });
  };

  const handleModalClose = () => {
    setState({
      ...state,
      isModalOpen: false,
    });
  };

  const handleChange = (field: string, e: any) => {
    const product = Object.assign({}, state.product, {
      [field]: e.target.value,
    });
    setState(Object.assign({}, state, { product }));
  };

  const handleNumericChange = (field: string, e: any) => {
    e.target.value = e.target.value.replace(/^0+/, "");
    const value = e.target.value ? e.target.valueAsNumber : 0;
    const product = Object.assign({}, state.product, { [field]: value });
    setState(Object.assign({}, state, { product }));
  };

  const onImageChange = (imageList: any, addUpdateIndex: any) => {
    const product = Object.assign({}, state.product, {
      image: imageList[0] ? imageList[0].data_url : "",
    });
    setState(Object.assign({}, state, { product }));
  };

  const submitHandler = (event: any) => {
    event.preventDefault();
    onUpdateProduct(state.product);
  };

  const deleteHandler = (productId: string) => {
    setState(updateObject(state, { redirectUrl: "/store/" + product.storeId }));
    onDeleteProduct(productId);
  };

  let form = (
    <>
      <ValidatorForm
        onSubmit={submitHandler}
        onError={(errors) => console.log(errors)}
      >
        <Grid item xs={12}>
          <TextValidator
            className="textfield"
            value={state.product.name}
            validators={["required"]}
            onChange={(event) => handleChange("name", event)}
            errorMessages={["field is required"]}
            id="name"
            label="Name"
            name="name"
          />
        </Grid>
        <Grid item xs={12}>
          <TextValidator
            className="textfield"
            value={state.product.description}
            validators={["required"]}
            onChange={(event) => handleChange("description", event)}
            errorMessages={["field is required"]}
            multiline
            id="description"
            label="Description"
            name="description"
          />
        </Grid>
        <Grid item xs={12}>
          <TextValidator
            type="number"
            className="textfield"
            value={state.product.price}
            validators={["required", "isGreaterThanZero"]}
            onChange={(event) => handleNumericChange("price", event)}
            errorMessages={[
              "field is required",
              "value needs to be greater than zero",
            ]}
            id="price"
            label="Price"
            name="price"
          />
        </Grid>
        <Grid item>
          <UploadImage
            onImageChange={onImageChange}
            image={state.product.image}
          />
        </Grid>
        <Grid item className="right top-padding">
          <Button
            color="secondary"
            style={{
              backgroundColor: "red",
            }}
            variant="contained"
            className="pull-left"
            startIcon={<DeleteIcon />}
            onClick={() => handleModalOpen()}
          >
            Delete
          </Button>
          <Button type="submit" variant="contained" color="primary">
            Update
          </Button>
        </Grid>
      </ValidatorForm>
      <YesNoModal
        open={state.isModalOpen}
        onClose={handleModalClose}
        onAccept={() => deleteHandler(product.id)}
        question="Are you sure you want to delete this product?"
      />
    </>
  );

  if (props.loading) {
    form = <Spinner />;
  }

  let errorMessage = null;

  if (props.error) {
    errorMessage = <p>{props.error.message}</p>;
  }

  let authRedirect = null;

  if (!props.isAuthenticated) {
    authRedirect = <Redirect to="/" />;
  }

  return (
    <Grid>
      <h1 className="header">Product</h1>
      <Paper className="login">
        {errorMessage}
        <Grid>
          {authRedirect}
          {form}
        </Grid>
      </Paper>
    </Grid>
  );
};

const mapStateToProps = (state: any) => {
  return {
    product: state.product.product,
    store: state.store.store,
    loading: state.product.loading,
    error: state.product.error,
    requestSent: state.product.requestSent,
    deletedSuccessful: state.product.deletedSuccessful,
    isAuthenticated: state.auth.token !== null,
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    onUpdateProduct: (product: any) => dispatch(actions.updateProduct(product)),
    onFetchProduct: (productId: string) =>
      dispatch(actions.fetchProduct(productId)),
    onDeleteProduct: (productId: string) =>
      dispatch(actions.deleteProduct(productId)),
    onSetAuthRedirectPath: () => dispatch(actions.setAuthRedirectPath("/")),
    onClearProduct: () => dispatch(actions.clearProduct()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditProduct);
