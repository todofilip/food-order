import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import {
  Grid,
  TextField,
  Paper,
  Button,
  makeStyles,
  Theme,
  createStyles,
} from "@material-ui/core";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";

import Spinner from "../../../UI/Spinner/Spinner";
import * as actions from "../../../../store/actions/index";
import { Redirect } from "react-router-dom";
import { Store } from "@material-ui/icons";
import UploadImage from "../../../UI/UploadImage/UploadImage";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      width: "100%",
    },
  })
);

interface CreateProductProps {
  product: any;
  store: any;
  loading: boolean;
  error: any;
  isAuthenticated: boolean;
  authRedirectPath: string;
  onCreateProduct(product: any): void;
  onClearProduct(): void;
}

const CreateProduct: React.SFC<CreateProductProps> = (props) => {
  const [state, setState] = useState({
    product: {
      name: "",
      description: "",
      price: 0,
      storeId: props.store ? props.store.id : null,
      image: ""
    },
  });

  const history = useHistory();
  const classes = useStyles();
  const { product } = props;
  const { onClearProduct } = props;

  useEffect(() => {
    ValidatorForm.addValidationRule("isGreaterThanZero", (value) => value > 0);

    return () => {
      ValidatorForm.removeValidationRule("isGreaterThanZero");
      onClearProduct();
    }
  }, []);

  useEffect(() => {
    if (props.product) {
      history.push("/store/" + product.storeId);
    }
  }, [props.product]);

  const handleChange = (field: string, e: any) => {
    const product = Object.assign({}, state.product, {
      [field]: e.target.value,
    });
    setState(Object.assign({}, state, { product }));
  };

  const handleNumericChange = (field: string, e: any) => {
    e.target.value = e.target.value.replace(/^0+/, "");
    const value = e.target.value ? e.target.valueAsNumber : 0;
    const product = Object.assign({}, state.product, { [field]: value });
    setState(Object.assign({}, state, { product }));
  };

  const onImageChange = (imageList: any, addUpdateIndex: any) => {
    const product = Object.assign({}, state.product, {
      image: imageList[0] ? imageList[0].data_url : "",
    });
    setState(Object.assign({}, state, { product }));
  };

  const submitHandler = (event: any) => {
    event.preventDefault();
    props.onCreateProduct(state.product);
  };

  let form = (
    <ValidatorForm
      onSubmit={submitHandler}
      onError={(errors) => console.log(errors)}
    >
      <Grid item xs={12}>
        <TextValidator
          className="textfield"
          value={state.product.name}
          validators={["required"]}
          onChange={(event) => handleChange("name", event)}
          errorMessages={["field is required"]}
          id="name"
          label="Name"
          name="name"
        />
      </Grid>
      <Grid item xs={12}>
        <TextValidator
          className="textfield"
          value={state.product.description}
          validators={["required"]}
          onChange={(event) => handleChange("description", event)}
          errorMessages={["field is required"]}
          multiline
          id="description"
          label="Description"
          name="description"
        />
      </Grid>
      <Grid item xs={12}>
        <TextValidator
          type="number"
          className="textfield"
          value={state.product.price}
          validators={["required", "isGreaterThanZero"]}
          onChange={(event) => handleNumericChange("price", event)}
          errorMessages={[
            "field is required",
            "value needs to be greater than zero",
          ]}
          id="price"
          label="Price"
          name="price"
        />
      </Grid>
      <Grid item>
        <UploadImage onImageChange={onImageChange} image={state.product.image} />
      </Grid>
      <Grid item className="right top-padding">
        <Button type="submit" variant="contained" color="primary">
          Create
        </Button>
      </Grid>
    </ValidatorForm>
  );

  if (props.loading) {
    form = <Spinner />;
  }

  let errorMessage = null;

  if (props.error) {
    errorMessage = <p>{props.error.message}</p>;
  }

  let authRedirect = null;

  if (!props.isAuthenticated) {
    authRedirect = <Redirect to="/" />;
  }

  return (
    <Grid>
      <h1 className="header">Product</h1>
      <Paper className="login">
        {errorMessage}
        <Grid>
          {authRedirect}
          {form}
        </Grid>
      </Paper>
    </Grid>
  );
};

const mapStateToProps = (state: any) => {
  return {
    product: state.product.product,
    store: state.store.store,
    loading: state.auth.loading,
    error: state.auth.error,
    isAuthenticated: state.auth.token !== null,
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    onCreateProduct: (product: any) => dispatch(actions.createProduct(product)),
    onClearProduct: () => dispatch(actions.clearProduct()),
    onSetAuthRedirectPath: () => dispatch(actions.setAuthRedirectPath("/")),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CreateProduct);
