import React from "react";
import {
  ListItem,
  ListItemText,
  Divider,
  ListItemAvatar,
  Avatar,
  Typography,
  makeStyles,
  Theme,
  createStyles,
} from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: "100%",
      maxWidth: "36ch",
      backgroundColor: theme.palette.background.paper,
    },
    inline: {
      display: "inline",
    },
    minOrder: {
      display: "inline",
      fontSize: 14,
    },
  })
);

interface ProductItemProps {
  product: any;
  loading: boolean;
  onProductClick(product: boolean): void;
}

const ProductItem: React.SFC<ProductItemProps> = (props) => {
  const classes = useStyles();
  const { product } = props;
  const { onProductClick } = props;

  return (
    <React.Fragment>
      <ListItem button onClick={() => onProductClick(product)}>
        <ListItemAvatar>
          <Avatar alt="product image" src={product.image} />
        </ListItemAvatar>
        <ListItemText
          primary={
            <Typography
              component="span"
              variant="body2"
              className={classes.inline}
              color="textPrimary"
            >
              {product.name}
            </Typography>
          }
          secondary={
            <React.Fragment>
              <Typography
                component="span"
                variant="body2"
                className={classes.inline}
                color="textSecondary"
              >
                {product && product.description ? product.description : ""}
              </Typography>
              <br />
              <Typography
                component="span"
                variant="body2"
                className={classes.minOrder}
                color="textSecondary"
              >
                {product && product.price
                  ? product.price.toFixed(2) + " RSD"
                  : ""}
              </Typography>
            </React.Fragment>
          }
        />
      </ListItem>
      <Divider variant="inset" />
    </React.Fragment>
  );
};

export default ProductItem;
