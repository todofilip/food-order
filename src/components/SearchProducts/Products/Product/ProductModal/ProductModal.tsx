import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import IconButton from "@material-ui/core/IconButton";
import AddCircleOutlineIcon from "@material-ui/icons/AddCircleOutline";
import RemoveCircleOutlineIcon from "@material-ui/icons/RemoveCircleOutline";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { makeStyles, Theme, createStyles } from "@material-ui/core";
import { connect } from "react-redux";

import * as actions from "../../../../../store/actions/index";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    price: {
      fontWeight: "bold",
    },
    input: {
      width: 50,
      textAlign: "right",
    },
  })
);

interface ProductModalProps {
  product: any;
  open: boolean;
  onClose(): void;
  onAddProductToCart(product: any): void;
}

const ProductModal: React.SFC<ProductModalProps> = (props) => {
  const [state, setState] = useState({
    quantity: 1,
  });

  const classes = useStyles();
  const { onClose } = props;
  const { product } = props;
  const { onAddProductToCart } = props;

  const onAddProductToCartClick = () => {
    product.quantity = state.quantity;
    onAddProductToCart(product);
    state.quantity = 1;
    onClose();
  };

  return (
    <div>
      <Dialog
        open={props.open}
        onClose={props.onClose}
        aria-labelledby="form-dialog-title"
      >
        {product ? <div style={{textAlign: 'center'}}><img style={{maxWidth:500}} src={product.image} /></div> : null}
        <DialogTitle id="form-dialog-title">
          {product ? product.name : null}
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            {product ? product.description : null}
          </DialogContentText>
          <DialogContentText className={classes.price}>
            {product ? product.price.toFixed(2) + " RSD" : null}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <IconButton
            color="primary"
            onClick={() => {
              if (state.quantity - 1 > 0) {
                setState({ ...state, quantity: --state.quantity });
              }
            }}
          >
            <RemoveCircleOutlineIcon />
          </IconButton>
          <TextField
            type="number"
            disabled
            value={state.quantity}
            className={classes.input}
          />
          <IconButton
            color="primary"
            onClick={() => setState({ ...state, quantity: ++state.quantity })}
          >
            <AddCircleOutlineIcon />
          </IconButton>
          <Button
            onClick={onAddProductToCartClick}
            color="primary"
            variant="contained"
          >
            Add to Cart
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    onAddProductToCart: (product: any) =>
      dispatch(actions.addProductToCart(product)),
    onRemoveProductFromCart: (id: string) =>
      dispatch(actions.removeProductFromCart(id)),
  };
};

export default connect(null, mapDispatchToProps)(ProductModal);
