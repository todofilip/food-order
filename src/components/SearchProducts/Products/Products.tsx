import React, { useEffect, useState } from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core";
import ProductItem from "./Product/ProductItem";
import { connect } from "react-redux";

import * as actions from "../../../store/actions/index";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      backgroundColor: theme.palette.background.paper,
      height: '100%'
    },
  }),
);

interface ProductsProps {
  products: any;
  loading: boolean;
  onProductClick(product: any): void;
  onClearProducts(): void;
}

const Products: React.SFC<ProductsProps> = (props) => {
  const classes = useStyles();
  const dummyProducts = [];
  const { onClearProducts } = props;

  useEffect(() => {
    return () => {
      onClearProducts();
    };
  }, []);

  for (let i = 0; i < 10; i++) {
    dummyProducts.push({ id: i });
  }

  return (
    <div className={classes.root}>
        {(props.products ? props.products : dummyProducts).map((product: any) => {
          return (
            <ProductItem
              key={product.id}
              product={product}
              loading={props.loading}
              onProductClick={props.onProductClick}
            />
          );
        })}
    </div>
  );
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    onClearProducts: () => dispatch(actions.clearProducts()),
  };
};

export default connect(null, mapDispatchToProps)(Products);
