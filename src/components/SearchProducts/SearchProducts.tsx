import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { makeStyles, createStyles, Theme } from "@material-ui/core";

import Products from "./Products/Products";
import Cart from "./Cart/Cart";
import * as actions from "../../store/actions/index";
import { RouteComponentProps } from "react-router-dom";
import ProductModal from "./Products/Product/ProductModal/ProductModal";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
      backgroundColor: theme.palette.background.default,
      height: '100%'
    },
  })
);

interface SearchProductsProps {
  products: any;
  error: any;
  loading: boolean;
  requestSent: boolean;
  showFilter: boolean;
  userId: string;
  token: string;
  onFetchProducts(restaurantId: string): void;
  onClearProducts(): void;
  onFetchOrder(orderId: string): void;
  onShowFilterChange(showFilter: boolean): void;
}

interface MatchParams {
  name: string;
}

interface SearchProductsProps extends RouteComponentProps<MatchParams> {}

const SearchProducts: React.SFC<SearchProductsProps> = (props) => {
  const [state, setState] = useState({
    product: null,
    loading: false,
    error: null,
    open: false,
  });

  const classes = useStyles();
  const { onFetchProducts } = props;
  const { onClearProducts } = props;
  const { onFetchOrder } = props;
  const { onShowFilterChange } = props;
  const { userId } = props;
  const { token } = props;
  const { showFilter } = props;
  let orderId = null;

  useEffect(() => {
    let params = props.match.params as any;

    if (params.id) {
      orderId = props.location.search ?  props.location.search.substring(1) : null;
      
      if (orderId) {
        onFetchOrder(orderId);
      }

      onFetchProducts(params.id);
    }

    return () => {
      onClearProducts();
    };
  }, []);

  const onProductClick = (product: any) => {
    handleModalOpen(product);
  };

  const handleModalOpen = (product: any) => {
    setState({
      ...state,
      open: true,
      product: product
    });
  };

  const handleModalClose = () => {
    setState({
      ...state,
      open: false
    });
  };

  const handleChange = (event: any) => {
    setState({
      ...state,
      [event.target.name]: event.target.value,
    });
  };

  const submitHandler = (event: any) => {
    event.preventDefault();
    onFetchProducts((props.match.params as any).id);

    if (showFilter) {
      onShowFilterChange(!showFilter);
    }
  };

  return (
    <div className={classes.root}>
      <Products
        products={props.products}
        loading={props.loading}
        onProductClick={onProductClick}
      />
      <Cart userId={userId} token={token} restaurantId={(props.match.params as any).id} />
      <ProductModal open={state.open} onClose={handleModalClose} product={state.product} />
    </div>
  );
};

const mapStateToProps = (state: any) => {
  return {
    products: state.products.products,
    error: state.restaurant.error,
    loading: state.restaurant.loading,
    requestSent: state.restaurant.requestSent,
    showFilter: state.restaurant.showFilter,
    userId: state.auth.userId,
    token: state.auth.token,
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    onFetchProducts: (restaurantId: string) =>
      dispatch(actions.fetchProducts(restaurantId)),
    onFetchOrder: (orderId: string) =>
      dispatch(actions.fetchOrder(orderId)),
    onClearProducts: () =>
      dispatch(actions.clearProducts()),
    onShowFilterChange: (showFilter: boolean) =>
      dispatch(actions.showFilterChange(showFilter)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchProducts);
