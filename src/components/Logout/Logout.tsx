import React, { useEffect } from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";

import * as actions from "../../store/actions/index";

interface LogoutProps {
  onLogout(): void;
  onEmptyCart(): void;
}

const Logout: React.SFC<LogoutProps> = (props) => {
  const { onLogout } = props;
  const { onEmptyCart } = props;

  useEffect(() => {
    onEmptyCart();
    onLogout();
  }, [onLogout]);

  return <Redirect to="/" />;
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    onLogout: () => dispatch(actions.logout()),
    onEmptyCart: () => dispatch(actions.emptyCart()),
  };
};

export default connect(null, mapDispatchToProps)(Logout);
