import React, { useEffect } from "react";
import {
  makeStyles,
  Theme,
  createStyles,
  Button,
  Typography,
} from "@material-ui/core";
import { connect } from "react-redux";

import * as actions from "../../store/actions/index";
import Spinner from "../UI/Spinner/Spinner";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      textAlign: "center",
      fontSize: 20,
      marginTop: 200,
      backgroundColor: theme.palette.background.default,
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
    },
    content: {
      flexGrow: 1,
      marginTop: theme.spacing(8),
    },
  })
);

interface SuccessProps {
  userId: string;
  isFavorite: boolean | null;
  addedSuccessful: boolean;
  onEmptyCart(): void;
  onCreateFavorite(favorite: any): void;
  onClearFavorite(): void;
}

const Success: React.SFC<SuccessProps> = (props: any) => {
  const classes = useStyles();
  const { userId } = props;
  const { isFavorite } = props;
  const { loading } = props;
  const { addedSuccessful } = props;
  const { onEmptyCart } = props;
  const { onClearFavorite } = props;
  const { onCreateFavorite } = props;
  const { onFetchFavorite } = props;

  useEffect(() => {
    const storeId = (props as any).match.params.id;
    onEmptyCart();
    onFetchFavorite(storeId, userId);

    return () => {
      onClearFavorite();
    };
  }, []);

  const onAddToFavorites = () => {
    const storeId = (props as any).match.params.id;

    onCreateFavorite({
      userId: userId,
      storeId: storeId,
    });
  };

  let favContent = null;

  if (loading) {
    favContent = <Spinner />;
  } else {
    if (addedSuccessful) {
      favContent = (
        <Typography component="span" variant="body1" color="textPrimary">
          Store has been added to your favorites successfuly
        </Typography>
      );
    } else if (isFavorite) {
      favContent = null;
    } else {
      favContent = (
        <Button variant="contained" color="primary" onClick={onAddToFavorites}>
          Add store to favorites
        </Button>
      );
    }
  }

  return (
    <div className={classes.root}>
      <Typography component="span" variant="body1" color="textPrimary">
        Your order has been sent successfuly!
      </Typography>
      <br />
      <br />
      {favContent}
    </div>
  );
};

const mapStateToProps = (state: any) => {
  return {
    userId: state.auth.userId,
    isFavorite: state.favorite.isFavorite,
    loading: state.favorite.loading,
    addedSuccessful: state.favorite.addedSuccessful,
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    onEmptyCart: () => dispatch(actions.emptyCart()),
    onClearFavorite: () => dispatch(actions.clearFavorite()),
    onCreateFavorite: (favorite: any) =>
      dispatch(actions.createFavorite(favorite)),
    onFetchFavorite: (storeId: string, userId: string) =>
      dispatch(actions.fetchFavorite(storeId, userId)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Success);
