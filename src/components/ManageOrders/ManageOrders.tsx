import React, { useEffect } from "react";
import { connect } from "react-redux";
import { makeStyles, createStyles, Theme } from "@material-ui/core";

import ManageOrder from "./ManageOrder/ManageOrderItem";
import Spinner from "../UI/Spinner/Spinner";
import * as actions from "../../store/actions/index";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      backgroundColor: "#fff",
      height: "100%",
    },
  })
);

interface ManageOrdersProps {
  orders: any;
  error: any;
  loading: boolean;
  requestSent: boolean;
  restaurants: any;
  userId: string;
  token: string;
  onFetchOrders(userId: string | null, token: string): void;
  onClearOrders(): void;
}

const ManageOrders: React.SFC<ManageOrdersProps> = (props) => {
  const classes = useStyles();
  const { orders } = props;
  const { restaurants } = props;
  const { onFetchOrders } = props;
  const { onClearOrders } = props;

  useEffect(() => {
    onFetchOrders(null, props.token);

    return () => {
      onClearOrders();
    };
  }, []);

  let content;

  if (orders) {
    content = orders.map((order: any) => {
      return (
        <ManageOrder key={order.id} order={order} restaurants={restaurants} />
      );
    });
  } else {
    content = <Spinner />;
  }

  return <div className={classes.root}>{content}</div>;
};

const mapStateToProps = (state: any) => {
  return {
    orders: state.order.orders,
    error: state.order.error,
    loading: state.order.loading,
    requestSent: state.order.requestSent,
    restaurants: state.restaurant.restaurants,
    userId: state.auth.userId,
    token: state.auth.token,
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    onFetchOrders: (userId: string, token: string) =>
      dispatch(actions.fetchOrders(userId, token)),
    onClearOrders: () => dispatch(actions.clearOrders()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ManageOrders);
