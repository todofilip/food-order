import React from "react";
import { useHistory } from "react-router-dom";
import {
  ListItem,
  ListItemText,
  Divider,
  Typography,
  makeStyles,
  Theme,
  createStyles,
  Button,
} from "@material-ui/core";
import { connect } from "react-redux";

import * as actions from "../../../store/actions/index";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: "100%",
      maxWidth: "36ch",
      backgroundColor: theme.palette.background.paper,
    },
    inline: {
      fontWeight: "bold",
    },
  })
);

interface ManageOrderItemProps {
  order: any;
  restaurants: any;
  onUpdateOrderStatus(orderId: string): void;
}

const ManageOrderItem: React.SFC<ManageOrderItemProps> = (props) => {
  const classes = useStyles();
  const { order } = props;
  const { onUpdateOrderStatus } = props;

  const getTotal = (items: any) => {
    return items.reduce(function (a: any, b: any) {
      return a + b.price * b.quantity;
    }, 0);
  };

  const formatDate = (dateString: any) => {
    let date = new Date(dateString);
    let dd = String(date.getDate()).padStart(2, "0");
    let mm = String(date.getMonth() + 1).padStart(2, "0");
    let yyyy = date.getFullYear();
    let hours = date.getHours();
    let minutes = date.getMinutes();
    return mm + "." + dd + "." + yyyy + " at " + hours + ":" + minutes;
  };

  const onOrderClick = () => {
    onUpdateOrderStatus(order.id);
  };

  const button = (
    <Button variant="contained" color="primary" onClick={onOrderClick}>
      Close
    </Button>
  );

  return (
    <React.Fragment>
      <ListItem>
        <ListItemText
          primary={
            <React.Fragment>
              <Typography
                component="span"
                variant="body2"
                className={classes.inline}
                color="textSecondary"
              >
                {order && order.storeName ? order.storeName : ""}
              </Typography>
              <br />
              <Typography
                component="span"
                variant="body2"
                color="textSecondary"
              >
                {order && order.date ? "Date: " + formatDate(order.date) : ""}
              </Typography>
            </React.Fragment>
          }
          secondary={
            <React.Fragment>
              <Typography
                component="span"
                variant="body2"
                color="textSecondary"
              >
                {order && order.username ? "Username: " + order.username : ""}
              </Typography>
              <br />
              <Typography
                component="span"
                variant="body2"
                color="textSecondary"
              >
                {order ? "Total: " + getTotal(order.orderLines) + " RSD" : null}
              </Typography>
              <br />
              <Typography
                component="span"
                variant="body2"
                style={
                  order && order.status && order.status === "delivered"
                    ? {
                        color: "limegreen",
                      }
                    : {
                        color: "orange",
                      }
                }
              >
                {order ? "Status: " + order.status : null}
              </Typography>
            </React.Fragment>
          }
        />
        {order && order.status === "ordered" ? button : null}
      </ListItem>
      <Divider variant="fullWidth" />
    </React.Fragment>
  );
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    onUpdateOrderStatus: (orderId: string) =>
      dispatch(actions.updateOrderStatus(orderId)),
  };
};

export default connect(null, mapDispatchToProps)(ManageOrderItem);
