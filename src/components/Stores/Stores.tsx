import React, { useEffect } from "react";
import { createStyles, Fab, makeStyles, Theme } from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import { useHistory } from "react-router-dom";
import { connect } from "react-redux";

import StoreItem from "./StoreItem/StoreItem";
import * as actions from "../../store/actions/index";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: "100%",
      backgroundColor: theme.palette.background.paper,
      height: "100%",
    },
    fab: {
      position: "fixed",
      bottom: theme.spacing(2),
      right: theme.spacing(2),
    },
    menuLink: {
      textDecoration: "none",
      color: theme.palette.text.primary,
    },
  })
);

interface StoresProps {
  restaurants: any;
  error: any;
  loading: boolean;
  requestSent: boolean;
  showFilter: boolean;
  userId: string;
  token: string;
  onFetchRestaurants(searchType: string): void;
  onClearRestaurants(): void;
}

const Stores: React.SFC<StoresProps> = (props) => {
  const classes = useStyles();
  const { onFetchRestaurants } = props;
  const { onClearRestaurants } = props;
  const dummyRestaurants = [];
  const history = useHistory();

  useEffect(() => {
    onFetchRestaurants("All");

    return () => {
      onClearRestaurants();
    };
  }, []);

  const redirect = () => {
    let path = "/store";
    history.push(path);
  };

  const onStoreClick = (store: any) => {
    let path = "/store/" + store.id;
    history.push(path);
  };

  for (let i = 0; i < 10; i++) {
    dummyRestaurants.push({ id: i });
  }

  return (
    <div className={classes.root}>
      {(props.restaurants ? props.restaurants : dummyRestaurants).map(
        (store: any) => {
          return (
            <StoreItem
              key={store.id}
              store={store}
              actionText={"Edit"}
              loading={props.loading}
              onStoreClick={onStoreClick}
            />
          );
        }
      )}
      <Fab
        color="secondary"
        className={classes.fab}
        aria-label="add"
        onClick={redirect}
      >
        <AddIcon />
      </Fab>
    </div>
  );
};

const mapStateToProps = (state: any) => {
  return {
    restaurants: state.restaurant.restaurants,
    error: state.restaurant.error,
    loading: state.restaurant.loading,
    requestSent: state.restaurant.requestSent,
    userId: state.auth.userId,
    token: state.auth.token,
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    onFetchRestaurants: (category: string) =>
      dispatch(actions.fetchRestaurants(category)),
    onClearRestaurants: () => dispatch(actions.clearRestaurants()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Stores);
