import React from "react";
import {
  ListItem,
  ListItemText,
  Divider,
  ListItemAvatar,
  Typography,
  makeStyles,
  Theme,
  createStyles,
  Button,
  useMediaQuery,
  useTheme,
} from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: "100%",
      maxWidth: "36ch",
      backgroundColor: theme.palette.background.paper,
    },
    inline: {
      display: "inline",
    },
    minOrder: {
      display: "inline",
      fontSize: 14,
    },
  })
);

interface StoreItemProps {
  store: any;
  actionText: string;
  loading: boolean;
  onStoreClick(store: any): void;
}

const StoreItem: React.SFC<StoreItemProps> = (props) => {
  const classes = useStyles();
  const theme = useTheme();
  const { store } = props;
  const { actionText } = props;
  const { onStoreClick } = props;
  const isMobile = !useMediaQuery(theme.breakpoints.up("md"));

  return (
    <React.Fragment>
      <ListItem>
        <ListItemAvatar>
          {store.image ? (
            <img
              alt="store image"
              style={{ height: 100, marginRight: 20, borderRadius: 10 }}
              src={store.image}
            />
          ) : (
            <div
              style={{ height: 100, marginRight: 20, borderRadius: 10 }}
            ></div>
          )}
        </ListItemAvatar>
        <ListItemText
          primary={store.name}
          secondary={
            <React.Fragment>
              <Typography
                component="span"
                variant="body1"
                className={classes.inline}
                color="textSecondary"
              >
                {store && store.category && store.address
                  ? store.category + (isMobile ? "" : " - " + store.address)
                  : ""}
              </Typography>
              <br />
              {isMobile ? null : (
                <Typography
                  component="span"
                  variant="body2"
                  className={classes.inline}
                  color="textSecondary"
                >
                  {store && store.description ? store.description : ""}
                </Typography>
              )}
            </React.Fragment>
          }
        />
        <Button
          variant="contained"
          color="primary"
          onClick={() => onStoreClick(props.store)}
        >
          {actionText}
        </Button>
      </ListItem>
      <Divider variant="fullWidth" />
    </React.Fragment>
  );
};

export default StoreItem;
