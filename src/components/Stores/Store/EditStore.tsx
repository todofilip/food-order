import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { useHistory } from "react-router-dom";
import {
  Grid,
  Paper,
  Button,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  makeStyles,
  Theme,
  createStyles,
  AppBar,
  Tabs,
  Tab,
  Fab,
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import AddIcon from "@material-ui/icons/Add";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";

import Spinner from "../../UI/Spinner/Spinner";
import * as actions from "../../../store/actions/index";
import { Redirect } from "react-router-dom";
import TabPanel from "../../UI/TabPanel/TabPanel";
import Products from "../../SearchProducts/Products/Products";
import UploadImage from "../../UI/UploadImage/UploadImage";
import YesNoModal from "../../UI/YesNoModal/YesNoModal";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      height: "100%",
    },
    formControl: {
      width: "100%",
    },
    fab: {
      position: "fixed",
      bottom: theme.spacing(2),
      right: theme.spacing(2),
    },
  })
);

interface EditStoreProps {
  store: any;
  products: any;
  loading: boolean;
  error: any;
  requestSent: boolean;
  deletedSuccessful: boolean;
  isAuthenticated: boolean;
  authRedirectPath: string;
  onFetchStore(storeId: string): void;
  onUpdateStore(store: any): void;
  onDeleteStore(storeId: string): void;
  onFetchProducts(storeId: string): void;
  onClearStore(): void;
  onSetAuthRedirectPath(): void;
}

const EditStore: React.SFC<EditStoreProps> = (props) => {
  const [state, setState] = useState({
    store: {
      id: "",
      name: "",
      address: "",
      description: "",
      category: "",
      image: "",
    },
    isModalOpen: false,
  });

  const [value, setValue] = React.useState(0);
  const classes = useStyles();
  const history = useHistory();
  const { requestSent } = props;
  const { loading } = props;
  const { deletedSuccessful } = props;
  const { store } = props;
  const { onFetchStore } = props;
  const { onUpdateStore } = props;
  const { onDeleteStore } = props;
  const { onFetchProducts } = props;
  const { onClearStore } = props;

  useEffect(() => {
    const id = (props as any).match.params.id;

    if (deletedSuccessful) {
      history.push("/stores");
      return;
    }

    if (!requestSent && !props.store) {
      onFetchStore(id);
    }

    if (props.store && !loading && !requestSent) {
      setState({ ...state, store: props.store });
    }
  }, [
    onFetchStore,
    props.store,
    requestSent,
    store,
    (props as any).match.params.id,
  ]);

  useEffect(() => {
    return () => {
      onClearStore();
    };
  }, []);

  const handleModalOpen = () => {
    setState({
      ...state,
      isModalOpen: true
    });
  };

  const handleModalClose = () => {
    setState({
      ...state,
      isModalOpen: false
    });
  };

  const handleChange = (field: string, e: any) => {
    const store = Object.assign({}, state.store, { [field]: e.target.value });
    setState(Object.assign({}, state, { store }));
  };

  const tabHandleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    if (newValue === 1) {
      onFetchProducts(props.store.id);
    }

    setValue(newValue);
  };

  const submitHandler = (event: any) => {
    event.preventDefault();
    onUpdateStore(state.store);
  };

  const onProductClick = (product: any) => {
    let path = "/product/" + product.id;
    history.push(path);
  };

  const onImageChange = (imageList: any, addUpdateIndex: any) => {
    const store = Object.assign({}, state.store, {
      image: imageList[0] ? imageList[0].data_url : "",
    });
    setState(Object.assign({}, state, { store }));
  };

  const redirectToCreateProduct = () => {
    history.push("/product");
  };

  const a11yProps = (index: any) => {
    return {
      id: `simple-tab-${index}`,
      "aria-controls": `simple-tabpanel-${index}`,
    };
  };
  
  let fields = (
    <div>
      <ValidatorForm
        onSubmit={submitHandler}
        onError={(errors) => console.log(errors)}
      >
        <Grid item xs={12}>
          <TextValidator
            className="textfield"
            value={state.store.name}
            validators={["required"]}
            onChange={(event) => {
              handleChange("name", event);
            }}
            errorMessages={["field is required"]}
            id="name"
            label="Name"
            name="name"
          />
        </Grid>
        <Grid item xs={12}>
          <TextValidator
            className="textfield"
            value={state.store.address}
            validators={["required"]}
            onChange={(event) => {
              handleChange("address", event);
            }}
            errorMessages={["field is required"]}
            id="address"
            label="Address"
            name="address"
          />
        </Grid>
        <Grid item xs={12}>
          <TextValidator
            className="textfield"
            value={state.store.description}
            validators={["required"]}
            onChange={(event) => {
              handleChange("description", event);
            }}
            multiline
            errorMessages={["field is required"]}
            id="description"
            label="Description"
            name="description"
          />
        </Grid>
        <Grid item>
          <FormControl className={classes.formControl}>
            <InputLabel>Category</InputLabel>
            <Select
              name="category"
              value={state.store.category}
              onChange={(event) => handleChange("category", event)}
            >
              <MenuItem value="All">All</MenuItem>
              <MenuItem value="Pizza">Pizza</MenuItem>
              <MenuItem value="Burgers">Burgers</MenuItem>
              <MenuItem value="Chicken">Chicken</MenuItem>
              <MenuItem value="Grill">Grill</MenuItem>
            </Select>
          </FormControl>
        </Grid>
        <Grid item>
          <UploadImage
            onImageChange={onImageChange}
            image={state.store.image}
          />
        </Grid>
        <Grid item className="right top-padding">
          <Button
            color="secondary"
            style={{
              backgroundColor: "red",
            }}
            variant="contained"
            className="pull-left"
            startIcon={<DeleteIcon />}
            onClick={() => handleModalOpen()}
          >
            Delete
          </Button>
          <Button type="submit" variant="contained" color="primary">
            Update
          </Button>
        </Grid>
      </ValidatorForm>
      <YesNoModal open={state.isModalOpen} onClose={handleModalClose} onAccept={() => onDeleteStore(store.id)} question="Are you sure you want to delete this store?" />
    </div>
  );

  if (props.loading) {
    fields = <Spinner />;
  }

  let errorMessage = null;

  if (props.error) {
    errorMessage = <p>{props.error.message}</p>;
  }

  let authRedirect = null;

  if (!props.isAuthenticated) {
    authRedirect = <Redirect to="/" />;
  }

  let form = (
    <Grid>
      <h1 className="header">Store</h1>
      <Paper className="login">
        {errorMessage}
        <Grid>
          {authRedirect}
          {fields}
        </Grid>
      </Paper>
    </Grid>
  );

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Tabs
          centered
          value={value}
          onChange={tabHandleChange}
          aria-label="simple tabs example"
        >
          <Tab label="Store info" {...a11yProps(0)} />
          <Tab label="Products" {...a11yProps(1)} />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0} style={null}>
        {form}
      </TabPanel>
      <TabPanel value={value} index={1} style={{
         height: 'calc(100% - 48px)',
         background: 'white'
      }}>
        <Products
          products={props.products}
          loading={props.loading}
          onProductClick={onProductClick}
        />
        <Fab
          color="secondary"
          className={classes.fab}
          aria-label="Add new product"
          onClick={redirectToCreateProduct}
        >
          <AddIcon />
        </Fab>
      </TabPanel>
    </div>
  );
};

const mapStateToProps = (state: any) => {
  return {
    store: state.store.store,
    requestSent: state.store.requestSent,
    loading: state.store.loading,
    error: state.store.error,
    deletedSuccessful: state.store.deletedSuccessful,
    products: state.products.products,
    isAuthenticated: state.auth.token !== null,
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    onUpdateStore: (store: any) => dispatch(actions.updateStore(store)),
    onDeleteStore: (storeId: any) => dispatch(actions.deleteStore(storeId)),
    onFetchStore: (storeId: string) => dispatch(actions.fetchStore(storeId)),
    onFetchProducts: (storeId: string) =>
      dispatch(actions.fetchProducts(storeId)),
    onClearStore: () => dispatch(actions.clearStore()),
    onSetAuthRedirectPath: () => dispatch(actions.setAuthRedirectPath("/")),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditStore);
