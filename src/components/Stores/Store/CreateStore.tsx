import React, { useEffect, useState } from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { useHistory } from "react-router-dom";
import {
  Grid,
  Paper,
  Button,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  makeStyles,
  Theme,
  createStyles,
} from "@material-ui/core";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";

import Spinner from "../../UI/Spinner/Spinner";
import * as actions from "../../../store/actions/index";
import UploadImage from "../../UI/UploadImage/UploadImage";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      width: "100%",
    },
  })
);

interface CreateStoreProps {
  store: any;
  loading: boolean;
  error: any;
  isAuthenticated: boolean;
  authRedirectPath: string;
  onCreateStore(store: any): void;
  onSetAuthRedirectPath(): void;
}

const CreateStore: React.SFC<CreateStoreProps> = (props) => {
  const [state, setState] = useState({
    store: {
      name: "",
      address: "",
      description: "",
      category: "All",
      image: "",
    },
  });

  const history = useHistory();
  const classes = useStyles();
  const [images, setImages] = React.useState([]);

  useEffect(() => {
    if (props.store) {
      let path = "/store/" + props.store.id;
      history.push(path);
    }
  }, [props.store]);

  const handleChange = (field: string, e: any) => {
    const store = Object.assign({}, state.store, { [field]: e.target.value });
    setState(Object.assign({}, state, { store }));
  };

  const onImageChange = (imageList: any, addUpdateIndex: any) => {
    const store = Object.assign({}, state.store, {
      image: imageList[0] ? imageList[0].data_url : "",
    });
    setState(Object.assign({}, state, { store }));
    setImages(imageList);
  };

  const submitHandler = (event: any) => {
    event.preventDefault();
    props.onCreateStore(state.store);
  };

  let form = (
    <ValidatorForm
      onSubmit={submitHandler}
      onError={(errors) => console.log(errors)}
    >
      <Grid item xs={12}>
        <TextValidator
          className="textfield"
          value={state.store.name}
          validators={["required"]}
          onChange={(event) => handleChange("name", event)}
          errorMessages={["field is required"]}
          id="name"
          label="Name"
          name="name"
        />
      </Grid>
      <Grid item xs={12}>
          <TextValidator
            className="textfield"
            value={state.store.address}
            validators={["required"]}
            onChange={(event) => {
              handleChange("address", event);
            }}
            errorMessages={["field is required"]}
            id="address"
            label="Address"
            name="address"
          />
        </Grid>
        <Grid item xs={12}>
          <TextValidator
            className="textfield"
            value={state.store.description}
            validators={["required"]}
            onChange={(event) => {
              handleChange("description", event);
            }}
            multiline
            errorMessages={["field is required"]}
            id="description"
            label="Description"
            name="description"
          />
        </Grid>
      <Grid item>
        <FormControl className={classes.formControl}>
          <InputLabel>Food type</InputLabel>
          <Select
            name="category"
            value={state.store.category}
            onChange={(event) => handleChange("category", event)}
          >
            <MenuItem value="All">All</MenuItem>
            <MenuItem value="Pizza">Pizza</MenuItem>
            <MenuItem value="Burgers">Burgers</MenuItem>
            <MenuItem value="Chicken">Chicken</MenuItem>
            <MenuItem value="Grill">Grill</MenuItem>
          </Select>
        </FormControl>
      </Grid>
      <Grid item>
        <UploadImage onImageChange={onImageChange} image={state.store.image} />
      </Grid>
      <Grid item className="right top-padding">
        <Button type="submit" variant="contained" color="primary">
          Create
        </Button>
      </Grid>
    </ValidatorForm>
  );

  if (props.loading) {
    form = <Spinner />;
  }

  let errorMessage = null;

  if (props.error) {
    errorMessage = <p>{props.error.message}</p>;
  }

  let authRedirect = null;

  if (!props.isAuthenticated) {
    authRedirect = <Redirect to="/" />;
  }

  return (
    <Grid>
      <h1 className="header">Store</h1>
      <Paper className="login">
        {errorMessage}
        <Grid>
          {authRedirect}
          {form}
        </Grid>
      </Paper>
    </Grid>
  );
};

const mapStateToProps = (state: any) => {
  return {
    store: state.store.store,
    loading: state.auth.loading,
    error: state.auth.error,
    isAuthenticated: state.auth.token !== null,
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    onCreateStore: (store: any) => dispatch(actions.createStore(store)),
    onSetAuthRedirectPath: () => dispatch(actions.setAuthRedirectPath("/")),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CreateStore);
