import React from "react";
import { Link } from "react-router-dom";
import {
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  makeStyles,
  createStyles,
  Menu,
  MenuItem,
  Theme,
  useMediaQuery,
  useTheme,
  CardMedia,
} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import FilterListIcon from "@material-ui/icons/FilterList";
import { connect } from "react-redux";

import * as actions from "../../store/actions/index";
import logo from "./logo.png";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    mediaLogo: {
      width: 40,
      height: "auto",
      marginRight: 10,
      // margin: "0 auto",
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
    },
    header: {
      textAlign: "center",
      // flexGrow: 1,
      margin: "0 auto",
    },
    title: {
      // flexGrow: 1,
      textAlign: "center",
    },
    headerLink: {
      textDecoration: "none",
      color: "#fff",
    },
    menuLink: {
      textDecoration: "none",
      color: theme.palette.text.primary,
    },
  })
);

interface HeaderProps {
  isAuthenticated: boolean;
  username: string;
  showFilter: boolean;
  isFilterVisible: boolean;
  onShowFilterChange(showFilter: boolean): void;
}

const Header: React.SFC<HeaderProps> = (props) => {
  const classes = useStyles();
  const theme = useTheme();
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const { showFilter } = props;
  const { onShowFilterChange } = props;
  const { username } = props;
  const { isFilterVisible } = props;
  const isMobile = !useMediaQuery(theme.breakpoints.up("md"));

  const handleMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClick = () => {
    onShowFilterChange(!showFilter);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const userMenu = (
    <div>
      <IconButton
        aria-label="account of current user"
        aria-controls="menu-appbar"
        aria-haspopup="true"
        onClick={handleMenu}
        color="inherit"
      >
        <MenuIcon />
      </IconButton>
      <Menu
        id="menu-appbar"
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        keepMounted
        transformOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        open={open}
        onClose={handleClose}
      >
        <Link to="/" className={classes.menuLink}>
          <MenuItem onClick={handleClose}>Restaurants</MenuItem>
        </Link>
        <Link to="/orders" className={classes.menuLink}>
          <MenuItem onClick={handleClose}>My orders</MenuItem>
        </Link>
        <Link to="/favorites" className={classes.menuLink}>
          <MenuItem onClick={handleClose}>My favorites</MenuItem>
        </Link>
        <Link to="/profile" className={classes.menuLink}>
          <MenuItem onClick={handleClose}>My profile</MenuItem>
        </Link>
        <Link to="/help" className={classes.menuLink}>
          <MenuItem onClick={handleClose}>Help & FAQ</MenuItem>
        </Link>
        <Link to="/logout" className={classes.menuLink}>
          <MenuItem onClick={handleClose}>Logout</MenuItem>
        </Link>
      </Menu>
    </div>
  );

  const adminMenu = (
    <div>
      <IconButton
        aria-label="account of current user"
        aria-controls="menu-appbar"
        aria-haspopup="true"
        onClick={handleMenu}
        color="inherit"
      >
        <MenuIcon />
      </IconButton>
      <Menu
        id="menu-appbar"
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        keepMounted
        transformOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        open={open}
        onClose={handleClose}
      >
        <Link to="/stores" className={classes.menuLink}>
          <MenuItem onClick={handleClose}>Manage stores</MenuItem>
        </Link>
        <Link to="/manageOrders" className={classes.menuLink}>
          <MenuItem onClick={handleClose}>Manage orders</MenuItem>
        </Link>
        <Link to="/logout" className={classes.menuLink}>
          <MenuItem onClick={handleClose}>Logout</MenuItem>
        </Link>
      </Menu>
    </div>
  );

  return (
    <AppBar position="fixed" className={classes.appBar}>
      <Toolbar>
        {isMobile && isFilterVisible && username !== "admin" ? (
          <IconButton
            edge="start"
            color="inherit"
            aria-label="menu"
            onClick={handleClick}
          >
            <FilterListIcon />
          </IconButton>
        ) : null}
        <div className={classes.header}>
          <div
            style={{
              display: "flex",
              alignItems: "center",
              flexWrap: "wrap",
            }}
          >
            <Link to="/">
              <img src={logo} className={classes.mediaLogo} />
            </Link>
            <p
              style={{
                fontSize: 17,
              }}
            >
              Food order
            </p>
          </div>
        </div>
        {/* <Typography variant="h6" className={classes.title}>
        
          <Link to="/restaurants" className={classes.headerLink}>
            Food order
          </Link>
        </Typography> */}

        {props.isAuthenticated
          ? username === "admin"
            ? adminMenu
            : userMenu
          : null}
      </Toolbar>
    </AppBar>
  );
};

const mapStateToProps = (state: any) => {
  return {
    showFilter: state.restaurant.showFilter,
    isFilterVisible: !!state.restaurant.restaurants,
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    onShowFilterChange: (showFilter: boolean) =>
      dispatch(actions.showFilterChange(showFilter)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
