import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import { Redirect } from "react-router-dom";
import { Grid, Paper, Button, FormGroup, FormControlLabel, Switch } from "@material-ui/core";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";

import Spinner from "../UI/Spinner/Spinner";
import * as actions from "../../store/actions/index";

interface RegisterProps {
  loading: boolean;
  error: any;
  createdSuccessful: boolean;
  isAuthenticated: boolean;
  authRedirectPath: string;
  onCreateUser(user: any): void;
  onClearUser(): void;
  onSetAuthRedirectPath(): void;
}

const Register: React.SFC<RegisterProps> = (props) => {
  const [state, setState] = useState({
    user: {
      username: "",
      password: "",
      firstName: "",
      lastName: "",
      useDarkTheme: false,
      repeatPassword: "",
    },
  });

  const history = useHistory();
  const { createdSuccessful } = props;
  const { onClearUser } = props;

  useEffect(() => {
    if (props.createdSuccessful) {
      history.push("/login");
      return;
    }
  }, [createdSuccessful]);

  useEffect(() => {
    ValidatorForm.addValidationRule("isPasswordMatch", (value) => {
      if (value !== state.user.password) {
        return false;
      }
      return true;
    });

    return () => {
      ValidatorForm.removeValidationRule("isPasswordMatch");
      onClearUser();
    };
  }, []);

  const handleChange = (event: any) => {
    const { user } = state;
    (user as any)[event.target.name] = event.target.value;
    setState({ user });
  };

  const handleSwitchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { user } = state;
    (user as any)[event.target.name] = event.target.checked;
    setState({ user });
  };

  const submitHandler = (event: any) => {
    event.preventDefault();
    props.onCreateUser(state);
  };

  let form = (
    <div>
      <Grid item xs={12}>
        <TextValidator
          className="textfield"
          value={state.user.username}
          validators={["required"]}
          onChange={handleChange}
          errorMessages={["field is required"]}
          id="username"
          label="Username"
          name="username"
        />
        <TextValidator
          className="textfield"
          label="Password"
          onChange={handleChange}
          name="password"
          type="password"
          validators={["required"]}
          errorMessages={["this field is required"]}
          value={state.user.password}
        />
        <TextValidator
          className="textfield"
          label="Repeat password"
          onChange={handleChange}
          name="repeatPassword"
          type="password"
          validators={["isPasswordMatch", "required"]}
          errorMessages={["password mismatch", "this field is required"]}
          value={state.user.repeatPassword}
        />
        <TextValidator
          className="textfield"
          value={state.user.firstName}
          validators={["required"]}
          onChange={handleChange}
          errorMessages={["field is required"]}
          id="firstName"
          label="First name"
          name="firstName"
        />
        <TextValidator
          className="textfield"
          value={state.user.lastName}
          validators={["required"]}
          onChange={handleChange}
          errorMessages={["field is required"]}
          id="lastName"
          label="Last name"
          name="lastName"
        />
        <FormGroup row>
          <FormControlLabel
            control={
              <Switch
                checked={state.user.useDarkTheme}
                onChange={handleSwitchChange}
                name="useDarkTheme"
                color="primary"
              />
            }
            label={
              <div>
                Use dark theme
              </div>
            }
          />
        </FormGroup>
      </Grid>
      <Grid item className="right top-padding">
        <Link to="/login">
          <Button color="primary" className="pull-left">
            Login
          </Button>
        </Link>
        <Button type="submit" variant="contained" color="primary">
          Register
        </Button>
      </Grid>
    </div>
  );

  if (props.loading) {
    form = <Spinner />;
  }

  let errorMessage = null;

  if (props.error) {
    errorMessage = <p>{props.error.message}</p>;
  }

  let authRedirect = null;

  if (props.isAuthenticated) {
    authRedirect = <Redirect to="/" />;
  }

  return (
    <Grid>
      <h1 className="header">Register</h1>
      <Paper className="login">
        {errorMessage}
        <Grid>
          {authRedirect}
          <ValidatorForm
            onSubmit={submitHandler}
            onError={(errors) => console.log(errors)}
          >
            {form}
          </ValidatorForm>
        </Grid>
      </Paper>
    </Grid>
  );
};

const mapStateToProps = (state: any) => {
  return {
    loading: state.user.loading,
    error: state.user.error,
    createdSuccessful: state.user.createdSuccessful,
    isAuthenticated: state.auth.token !== null,
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    onCreateUser: (user: any) => dispatch(actions.createUser(user)),
    onClearUser: () => dispatch(actions.clearUser()),
    onSetAuthRedirectPath: () => dispatch(actions.setAuthRedirectPath("/")),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Register);
