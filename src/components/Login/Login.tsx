import React, { useState } from "react";
import { Redirect, Link } from "react-router-dom";
import { connect } from "react-redux";
import {
  Grid,
  TextField,
  Paper,
  Button,
  Input,
  FormControl,
  InputLabel,
  CircularProgress,
} from "@material-ui/core";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";

import * as actions from "../../store/actions/index";
import "./Login.css";

interface LoginProps {
  loading: boolean;
  error: any;
  isAuthenticated: boolean;
  authRedirectPath: string;
  onAuth(username: string, password: string): void;
  onSetAuthRedirectPath(): void;
}

const Login: React.SFC<LoginProps> = (props) => {
  const [state, setState] = useState({
    username: "",
    password: "",
    showPassword: false,
  });

  const submitHandler = (event: any) => {
    event.preventDefault();
    props.onAuth(state.username, state.password);
  };

  let form = (
    <div>
      <Grid item xs={12}>
        <TextField
          className="textfield"
          value={state.username}
          onChange={(event) => {
            setState({ ...state, username: event.target.value });
          }}
          id="username"
          label="Username"
        />
        <FormControl className="textfield">
          <InputLabel htmlFor="standard-adornment-password">
            Password
          </InputLabel>
          <Input
            type={state.showPassword ? "text" : "password"}
            value={state.password}
            onChange={(event) => {
              setState({ ...state, password: event.target.value });
            }}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={() => {
                    setState({ ...state, showPassword: !state.showPassword });
                  }}
                  onMouseDown={(event: any) => {
                    event.preventDefault();
                  }}
                >
                  {state.showPassword ? <Visibility /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            }
          />
        </FormControl>
      </Grid>
      <Grid item className="right top-padding">
        <Link to="/register">
          <Button color="primary" className="pull-left">
            Register
          </Button>
        </Link>
        <Button type="submit" variant="contained" color="primary">
          Login
        </Button>
      </Grid>
    </div>
  );

  if (props.loading) {
    form = (
      <div className="center padding-50">
        <CircularProgress />
      </div>
    );
  }

  let errorMessage = null;

  if (props.error) {
    errorMessage = <p>{props.error.message}</p>;
  }

  let authRedirect = null;

  if (props.isAuthenticated) {
    authRedirect = <Redirect to="/" />;
  }

  return (
    <Grid>
      <h1 className="header">Login</h1>
      <Paper className="login">
        <Grid>
          {authRedirect}
          {errorMessage}
          <form
            onSubmit={(event) => submitHandler(event)}
            noValidate
            autoComplete="off"
          >
            {form}
          </form>
        </Grid>
      </Paper>
    </Grid>
  );
};

const mapStateToProps = (state: any) => {
  return {
    loading: state.auth.loading,
    error: state.auth.error,
    isAuthenticated: state.auth.token !== null,
    authRedirectPath: state.auth.authRedirectPath,
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    onAuth: (username: string, password: string) =>
      dispatch(actions.auth(username, password)),
    onSetAuthRedirectPath: () => dispatch(actions.setAuthRedirectPath("/")),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
