import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import {
  Drawer,
  Toolbar,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Typography,
  Grid,
  Button,
} from "@material-ui/core";
import {
  createStyles,
  Theme,
  makeStyles,
  useTheme,
} from "@material-ui/core/styles";
import * as actions from "../../../store/actions/index";
import useMediaQuery from "@material-ui/core/useMediaQuery";

const drawerWidth = 240;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
    },
    drawer: {
      [theme.breakpoints.up("md")]: {
        width: drawerWidth,
        flexShrink: 0,
      },
    },
    drawerPaper: {
      width: drawerWidth,
    },
    drawerContainer: {
      overflow: "auto",
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: 160,
    },
    filterTitle: {
      marginTop: theme.spacing(4),
      fontSize: 18,
      width: 140,
      textAlign: "center",
    },
  })
);

interface FilterProps {
  showFilter?: boolean;
  searchType: string;
  handleChange?: any;
  submitHandler?: any;
  onShowFilterChange(showFilter: boolean): void;
}

const Filter: React.SFC<FilterProps> = (props) => {
  const classes = useStyles();
  const theme = useTheme();
  const isDesktop = useMediaQuery(theme.breakpoints.up("md"));
  const { showFilter } = props;
  const { onShowFilterChange } = props;
  const onClose = () => {
    onShowFilterChange(!showFilter);
  }

  return (
    <Drawer
      className={classes.drawer}
      variant={isDesktop ? "permanent" : "temporary"}
      open={showFilter}
      onClose={onClose}
      classes={{
        paper: classes.drawerPaper,
      }}
    >
      <Toolbar />
      <form
        onSubmit={(event) => props.submitHandler(event)}
        noValidate
        autoComplete="off"
      >
        <div className={classes.drawerContainer}>
          <Grid container justify="center">
            <Grid item>
              <Typography
                variant="h5"
                component="h5"
                className={classes.filterTitle}
                color="textSecondary"
                gutterBottom
              >
                Filter
              </Typography>
            </Grid>
            <Grid item>
              <FormControl className={classes.formControl}>
                <InputLabel>Food type</InputLabel>
                <Select
                  name="searchType"
                  value={props.searchType}
                  onChange={props.handleChange}
                >
                  <MenuItem value="All">All</MenuItem>
                  <MenuItem value="Pizza">Pizza</MenuItem>
                  <MenuItem value="Burgers">Burgers</MenuItem>
                  <MenuItem value="Chicken">Chicken</MenuItem>
                  <MenuItem value="Grill">Grill</MenuItem>
                </Select>
              </FormControl>
            </Grid>
            <Grid item className="padding-10">
              <Button
                type="submit"
                variant="contained"
                color="primary"
              >
                Search
              </Button>
            </Grid>
          </Grid>
        </div>
      </form>
    </Drawer>
  );
};

const mapStateToProps = (state: any) => {
  return {
    showFilter: state.restaurant.showFilter,
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    onShowFilterChange: (showFilter: boolean) =>
      dispatch(actions.showFilterChange(showFilter)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Filter);
