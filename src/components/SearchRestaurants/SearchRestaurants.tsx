import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { makeStyles, createStyles, Theme } from "@material-ui/core";

import Restaurants from "./Restaurants/Restaurants";
import Filter from "./Filter/Filter";
import * as actions from "../../store/actions/index";
import { useHistory } from "react-router";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
      backgroundColor: theme.palette.background.default,
      height: "100%"
    },
  })
);

interface SearchRestaurantsProps {
  restaurants: any;
  error: any;
  loading: boolean;
  requestSent: boolean;
  showFilter: boolean;
  userId: string;
  token: string;
  onFetchRestaurants(category: string): void;
  onClearRestaurants(): void;
  onShowFilterChange(showFilter: boolean): void;
}

const SearchRestaurants: React.SFC<SearchRestaurantsProps> = (props) => {
  const [state, setState] = useState({
    loading: false,
    error: null,
    searchType: "All",
  });

  const classes = useStyles();
  const history = useHistory();
  const { onFetchRestaurants } = props;
  const { onClearRestaurants } = props;
  const { onShowFilterChange } = props;
  const { showFilter } = props;
  const { error } = props;

  useEffect(() => {
    onFetchRestaurants(state.searchType);

    return () => {
      onClearRestaurants();
    };
  }, []);

  const onRestaurantClick = (restaurant: any) => {
    history.push("restaurant/" + restaurant.id);
  };

  const handleChange = (event: any) => {
    setState({
      ...state,
      [event.target.name]: event.target.value,
    });
  };

  const submitHandler = (event: any) => {
    event.preventDefault();
    onFetchRestaurants(state.searchType);

    if (showFilter) {
      onShowFilterChange(!showFilter);
    }
  };

  if (error) {
    return <div style={{textAlign: 'center'}}>{error.message}</div>;
  }

  return (
    <div className={classes.root}>
      <Filter
        searchType={state.searchType}
        handleChange={handleChange}
        submitHandler={submitHandler}
      />
      <Restaurants
        restaurants={props.restaurants}
        loading={props.loading}
        onRestaurantClick={onRestaurantClick}
      />
    </div>
  );
};

const mapStateToProps = (state: any) => {
  return {
    restaurants: state.restaurant.restaurants,
    error: state.restaurant.error,
    loading: state.restaurant.loading,
    requestSent: state.restaurant.requestSent,
    showFilter: state.restaurant.showFilter,
    userId: state.auth.userId,
    token: state.auth.token,
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    onFetchRestaurants: (category: string) =>
      dispatch(actions.fetchRestaurants(category)),
    onClearRestaurants: () =>
      dispatch(actions.clearRestaurants()),
    onShowFilterChange: (showFilter: boolean) =>
      dispatch(actions.showFilterChange(showFilter)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchRestaurants);
