import React from "react";
import {
  ListItem,
  ListItemText,
  Divider,
  ListItemAvatar,
  Typography,
  makeStyles,
  Theme,
  createStyles,
  Button,
  useMediaQuery,
  useTheme,
} from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: "100%",
      maxWidth: "36ch",
      backgroundColor: theme.palette.background.paper,
    },
    inline: {
      display: "inline",
    },
    minOrder: {
      display: "inline",
      fontSize: 14,
    },
  })
);

interface RestaurantProps {
  restaurant: any;
  loading: boolean;
  onRestaurantClick(restaurant: any): void;
}

const Restaurant: React.SFC<RestaurantProps> = (props) => {
  const classes = useStyles();
  const theme = useTheme();
  const { restaurant } = props;
  const { onRestaurantClick } = props;
  const isMobile = !useMediaQuery(theme.breakpoints.up("md"));

  return (
    <React.Fragment>
      <ListItem>
        <ListItemAvatar>
          {restaurant.image ? (
            <img
              alt="restaurant image"
              style={{ height: 100, marginRight: 20, borderRadius: 10 }}
              src={restaurant.image}
            />
          ) : (
            <div
              style={{ height: 100, marginRight: 20, borderRadius: 10 }}
            ></div>
          )}
        </ListItemAvatar>
        <ListItemText
          primary={
            <Typography
              component="span"
              variant="body1"
              className={classes.inline}
              color="textPrimary"
            >
              {restaurant.name}
            </Typography>
          }
          secondary={
            <React.Fragment>
              <Typography
                component="span"
                variant="body1"
                className={classes.inline}
                color="textSecondary"
              >
                {restaurant && restaurant.category && restaurant.address
                  ? restaurant.category +
                    (isMobile ? "" : " - " + restaurant.address)
                  : ""}
              </Typography>
              <br />
              {isMobile ? null : (
                <Typography
                  component="span"
                  variant="body2"
                  className={classes.inline}
                  color="textSecondary"
                >
                  {restaurant && restaurant.description
                    ? restaurant.description
                    : ""}
                </Typography>
              )}
            </React.Fragment>
          }
        />
        <Button
          variant="contained"
          color="primary"
          onClick={() => onRestaurantClick(props.restaurant)}
        >
          Order
        </Button>
      </ListItem>
      <Divider variant="fullWidth" />
    </React.Fragment>
  );
};

export default Restaurant;
