import React from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core";
import { v4 as uuidv4 } from "uuid";

import Restaurant from "./Restaurant/Restaurant";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: "100%",
      backgroundColor: theme.palette.background.paper,
    },
  })
);

interface RestaurantsProps {
  restaurants: any;
  loading: boolean;
  onRestaurantClick(restaurant: any): void;
}

const Restaurants: React.SFC<RestaurantsProps> = (props) => {
  const classes = useStyles();
  const { onRestaurantClick } = props;
  const dummyRestaurants = [];

  for (let i = 0; i < 10; i++) {
    dummyRestaurants.push({ id: i });
  }

  return (
    <div className={classes.root}>
      {(props.restaurants ? props.restaurants : dummyRestaurants).map(
        (restaurant: any) => {
          return (
            <Restaurant
              key={uuidv4()}
              restaurant={restaurant}
              loading={props.loading}
              onRestaurantClick={onRestaurantClick}
            />
          );
        }
      )}
    </div>
  );
};

export default Restaurants;
