import React from "react";
import { Link } from "react-router-dom";
import {
  Card,
  makeStyles,
  CardActions,
  Theme,
  createStyles,
  CardMedia,
  IconButton,
  CardHeader,
} from "@material-ui/core";
import FavoriteIcon from "@material-ui/icons/Favorite";
import Skeleton from "@material-ui/lab/Skeleton";

import "./FavoriteItem.css";
import noImage from "./no-image.png";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    card: {
      textAlign: "center",
      color: theme.palette.text.secondary,
    },
    media: {
      height: 200,
    },
    mediaNoImage: {
      height: 200,
      width: "auto",
      margin: "0 auto",
    },
    types: {
      textAlign: "right",
      width: "100%",
    },
    circleMargin: {
      margin: 4,
    },
  })
);

interface FavoriteItemProps {
  className: any;
  favorite: any;
  loading: boolean;
  isFavorite: boolean;
  onRemoveFavorite(favorite: any): void;
}

const FavoriteItem: React.SFC<FavoriteItemProps> = (props) => {
  const classes = useStyles();
  const { favorite } = props;
  const { loading = false } = props;
  const { onRemoveFavorite } = props;

  return (
    <div className={props.className}>
      <Card className={classes.card}>
        <CardHeader
          title={
            loading ? (
              <Skeleton height={10} width="100%" style={{ marginBottom: 6 }} />
            ) : (
              props.favorite.storeName
            )
          }
        />
        {loading ? (
          <Skeleton variant="rect" className={classes.media} />
        ) : (
          <React.Fragment>
            <Link to={"restaurant/" + favorite.storeId}>
              <CardMedia
                component="img"
                className={
                  favorite.storeImage ? classes.media : classes.mediaNoImage
                }
                image={favorite.storeImage ? favorite.storeImage : noImage}
                title="Photo title"
                alt="Photo"
                height="140"
              />
            </Link>
          </React.Fragment>
        )}
        <CardActions disableSpacing>
          {loading ? (
            <React.Fragment>
              <Skeleton
                variant="circle"
                width={40}
                height={40}
                className={classes.circleMargin}
              />
              <Skeleton
                variant="circle"
                width={40}
                height={40}
                className={classes.circleMargin}
              />
            </React.Fragment>
          ) : (
            <React.Fragment>
              <div style={{ marginLeft: 10 }}>{favorite.storeCategory}</div>
              <div className={classes.types}>
                <IconButton
                  aria-label="add to favorites"
                  onClick={() => onRemoveFavorite(props.favorite)}
                >
                  <FavoriteIcon htmlColor={"red"} />
                </IconButton>
              </div>
            </React.Fragment>
          )}
        </CardActions>
      </Card>
    </div>
  );
};

export default FavoriteItem;
