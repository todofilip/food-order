import React, { useEffect } from "react";
import { Grid, makeStyles, Theme, createStyles } from "@material-ui/core";
import { connect } from "react-redux";

import FavoriteItem from "./FavoriteItem/FavoriteItem";
import * as actions from "../../store/actions/index";

interface FavoritesProps {
  favorites: any;
  loading: boolean;
  requestSent: boolean;
  error: any;
  userId: string;
  onFetchFavorites(userId: string): void;
  onClearFavorites(): void;
  onDeleteFavorite(favoriteId: string): void;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      margin: theme.spacing(3),
    },
    wrapper: {
      marginBottom: 0,
    },
    paper: {
      textAlign: "center",
      color: theme.palette.text.secondary,
    },
  })
);

const Favorites: React.SFC<FavoritesProps> = (props) => {
  const classes = useStyles();
  const { userId } = props;
  const { onFetchFavorites } = props;
  const { onClearFavorites } = props;
  const { onDeleteFavorite } = props;
  const dummyFavorites = [];
  
  useEffect(() => {
    onFetchFavorites(userId);

    return () => {
      onClearFavorites();
    };
  }, []);

  for (let i = 0; i < 4; i++) {
    dummyFavorites.push({ id: i });
  }

  return (
    <div className={classes.root}>
      <Grid
        container
        spacing={3}
        justify="flex-start"
        className={classes.wrapper}
      >
        {(props.favorites ? props.favorites : dummyFavorites).map(
          (favorite: any) => {
            return (
              <Grid
                item
                xs={12}
                sm={6}
                md={4}
                lg={3}
                xl={3}
                className={classes.paper}
                key={favorite.id}
              >
                <FavoriteItem
                  key={favorite.favorite_id}
                  className={classes.paper}
                  favorite={favorite}
                  isFavorite={favorite.isFavorite}
                  loading={props.loading}
                  onRemoveFavorite={() => {
                    onDeleteFavorite(favorite.id);
                  }}
                />
              </Grid>
            );
          }
        )}
      </Grid>
    </div>
  );
};

const mapStateToProps = (state: any) => {
  return {
    favorites: state.favorites.favorites,
    error: state.favorites.error,
    loading: state.favorites.loading,
    requestSent: state.favorites.requestSent,
    userId: state.auth.userId,
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    onFetchFavorites: (userId: string) =>
      dispatch(actions.fetchFavorites(userId)),
    onClearFavorites: () => dispatch(actions.clearFavorites()),
    onDeleteFavorite: (favoriteId: string) =>
      dispatch(actions.deleteFavorite(favoriteId)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Favorites);
