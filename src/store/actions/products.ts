import axios from "../../shared/axios";
import * as actionTypes from "./actionTypes";

export const fetchProductsStart = () => {
  return {
    type: actionTypes.FETCH_PRODUCTS_START,
  };
};

export const fetchProductsSuccess = (products: any) => {
  return {
    type: actionTypes.FETCH_PRODUCTS_SUCCESS,
    products: products,
  };
};

export const fetchProductsFail = (error: any) => {
  return {
    type: actionTypes.FETCH_PRODUCTS_FAIL,
    error: error,
  };
};

export const showProductFilterChange = (showFilter: boolean) => {
  return {
    type: actionTypes.SHOW_PRODUCT_FILTER_CHANGE,
    showFilter: showFilter,
  };
};

export const fetchProducts = (storeId: string) => {
  return async (dispatch: any) => {
    dispatch(fetchProductsStart());

    axios
      .get("api/products/GetByStoreId/" + storeId)
      .then((response) => {
        dispatch(fetchProductsSuccess(response.data));
      })
      .catch((error) => {
        dispatch(fetchProductsFail(error));
      });
  };
};

export const clearProducts = () => {
  return (dispatch: any) => {
    dispatch(fetchProductsSuccess(null));
  };
};
