import { v4 as uuidv4 } from "uuid";

import * as actionTypes from "./actionTypes";
import axios from "../../shared/axios";

declare let localStorage: any;

export const fetchOrderStart = () => {
  return {
    type: actionTypes.FETCH_ORDER_START,
  };
};

export const fetchOrderSuccess = (orderId: any) => {
  return {
    type: actionTypes.FETCH_ORDER_SUCCESS,
    orderId: orderId,
  };
};

export const fetchOrderFail = (error: any) => {
  return {
    type: actionTypes.FETCH_ORDER_FAIL,
    error: error,
  };
};

export const fetchOrder = (orderId: string | null) => {
  return async (dispatch: any) => {
    dispatch(fetchOrderStart());

    axios
      .get("http://localhost:62296/api/orders/" + orderId)
      .then((response: any) => {
        dispatch(fetchOrderSuccess(orderId));
        localStorage.setItem(
          "productsInCart",
          JSON.stringify(response.data.orderLines)
        );
        dispatch(updateCart(response.data.orderLines));
      })
      .catch((error) => {
        dispatch(fetchOrderFail(error));
      });
  };
};

export const syncProductsInCart = () => {
  return (dispatch: any) => {
    let products = JSON.parse(localStorage.getItem("productsInCart"));

    if (!products) {
      products = [];
    }

    dispatch(updateCart(products));
  };
};

export const emptyCart = () => {
  return (dispatch: any) => {
    localStorage.setItem("productsInCart", JSON.stringify([]));
    dispatch(createNewCart());
  };
};

export const addProductToCart = (product: any) => {
  return (dispatch: any) => {
    product.guid = uuidv4();

    let products = JSON.parse(localStorage.getItem("productsInCart"));

    if (!products) {
      products = [];
    }

    let existingProduct = products.find(
      (element: any) => element.productId === product.id
    );

    if (existingProduct) {
      existingProduct.quantity += product.quantity;
    } else {
      products.push({
        productId: product.id,
        productName: product.name,
        price: product.price,
        quantity: product.quantity,
      });
    }

    localStorage.setItem("productsInCart", JSON.stringify(products));
    dispatch(updateCart(products));
  };
};

export const removeProductFromCart = (id: string) => {
  return (dispatch: any) => {
    let products = JSON.parse(localStorage.getItem("productsInCart"));

    if (!products) {
      return;
    }

    let product = products.find((product: any) => product.productId === id);

    if (!product) {
      return;
    } else if (product.quantity === 1) {
      let index = products.indexOf(product);

      if (index < 0) {
        throw Error("there is no such product in cart");
      }

      products.splice(index, 1);
    } else if (product.quantity > 1) {
      product.quantity = product.quantity - 1;
    }

    localStorage.setItem("productsInCart", JSON.stringify(products));
    dispatch(updateCart(products));
  };
};

export const removeProductsFromCart = (id: string) => {
  return (dispatch: any) => {
    const products = JSON.parse(localStorage.getItem("productsInCart"));

    if (!products) {
      throw Error("there is no products in cart");
    }

    const product = products.find((product: any) => product.productId === id);

    if (!product) {
      throw Error("there is no such product in cart");
    }

    const index = products.indexOf(product);

    if (index < 0) {
      throw Error("there is no such product in cart");
    }

    products.splice(index, 1);
    localStorage.setItem("productsInCart", JSON.stringify(products));

    dispatch(updateCart(products));
  };
};

export const removeAllProductsFromCart = () => {
  return (dispatch: any) => {
    localStorage.removeItem("productsInCart");
    dispatch(updateCart([]));
  };
};

export const updateCart = (products: any) => {
  return {
    type: actionTypes.UPDATE_CART,
    productsInCart: products,
  };
};

export const createNewCart = () => {
  return {
    type: actionTypes.CREATE_NEW_CART,
  };
};

export const postOrderStart = () => {
  return {
    type: actionTypes.POST_ORDER_START,
  };
};

export const postOrderSuccess = () => {
  return {
    type: actionTypes.POST_ORDER_SUCCESS,
  };
};

export const postOrderFail = (error: any) => {
  return {
    type: actionTypes.POST_ORDER_FAIL,
    error: error,
  };
};

export const postOrder = (
  products: any,
  restaurantId: string,
  userId: string
) => {
  return (dispatch: any, getState: any) => {
    dispatch(postOrderStart());

    const order: any = {
      id: getState().cart.orderId,
      orderLines: products,
      storeId: restaurantId,
      userId: userId,
      date: new Date(),
      status: "ordered",
    };

    if (order.id) {
      const url = "api/orders/" + order.id;

      axios
        .put(url, order)
        .then(() => {
          dispatch(postOrderSuccess());
        })
        .catch((error: any) => {
          dispatch(postOrderFail(error));
        });
    } else {
      axios
        .post("api/orders/", order)
        .then(() => {
          dispatch(postOrderSuccess());
        })
        .catch((error: any) => {
          dispatch(postOrderFail(error));
        });
    }
  };
};
