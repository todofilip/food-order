import axios from "../../shared/axios";
import * as actionTypes from "./actionTypes";

export const createUserStart = () => {
  return {
    type: actionTypes.CREATE_USER_START,
  };
};

export const createUserSuccess = (user: any) => {
  return {
    type: actionTypes.CREATE_USER_SUCCESS,
    user: user,
  };
};

export const createUserFail = (error: any) => {
  return {
    type: actionTypes.CREATE_USER_FAIL,
    error: error,
  };
};

export const createUser = (user: any) => {
  return (dispatch: any) => {
    dispatch(createUserStart());

    axios
      .post("api/users", user.user)
      .then((response) => {
        dispatch(createUserSuccess(response.data));
      })
      .catch((error) => {
        dispatch(createUserFail(error));
      });
  };
};

export const fetchUserStart = () => {
  return {
    type: actionTypes.FETCH_USER_START,
  };
};

export const fetchUserSuccess = (user: any) => {
  return {
    type: actionTypes.FETCH_USER_SUCCESS,
    user: user,
  };
};

export const fetchUserFail = (error: any) => {
  return {
    type: actionTypes.FETCH_USER_FAIL,
    error: error,
  };
};

export const fetchUser = (userId: string) => {
  return (dispatch: any) => {
    dispatch(fetchUserStart());

    axios
      .get("api/users/" + userId)
      .then((response) => {
        dispatch(fetchUserSuccess(response.data));
      })
      .catch((error) => {
        dispatch(fetchUserFail(error));
      });
  };
};

export const updateUserStart = () => {
  return {
    type: actionTypes.UPDATE_USER_START,
  };
};

export const updateUserSuccess = (user: any) => {
  return {
    type: actionTypes.UPDATE_USER_SUCCESS,
    user: user,
  };
};

export const updateUserFail = (error: any) => {
  return {
    type: actionTypes.UPDATE_USER_FAIL,
    error: error,
  };
};

export const updateUser = (user: any) => {
  return (dispatch: any) => {
    dispatch(updateUserStart());

    axios
      .put("api/users/" + user.id, user)
      .then((response) => {
        dispatch(updateUserSuccess(response.data));
      })
      .catch((error) => {
        dispatch(updateUserFail(error));
      });
  };
};

export const clearUser = () => {
  return (dispatch: any) => {
    dispatch(fetchUserSuccess(null));
  };
};
