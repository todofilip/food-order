import axios from "../../shared/axios";
import * as actionTypes from "./actionTypes";
import { removeFavorite } from "./favorites";

export const createFavoriteStart = () => {
  return {
    type: actionTypes.CREATE_FAVORITE_START,
  };
};

export const createFavoriteSuccess = () => {
  return {
    type: actionTypes.CREATE_FAVORITE_SUCCESS,
    isFavorite: true,
  };
};

export const createFavoriteFail = (error: any) => {
  return {
    type: actionTypes.CREATE_FAVORITE_FAIL,
    error: error,
  };
};

export const fetchFavoriteStart = () => {
  return {
    type: actionTypes.FETCH_FAVORITE_START,
  };
};

export const fetchFavoriteSuccess = (isFavorite: any) => {
  return {
    type: actionTypes.FETCH_FAVORITE_SUCCESS,
    isFavorite: isFavorite,
  };
};

export const fetchFavoriteFail = (error: any) => {
  return {
    type: actionTypes.FETCH_FAVORITE_FAIL,
    error: error,
  };
};

export const deleteFavoriteStart = () => {
  return {
    type: actionTypes.DELETE_FAVORITE_START,
  };
};

export const deleteFavoriteSuccess = () => {
  return {
    type: actionTypes.DELETE_FAVORITE_SUCCESS,
  };
};

export const deleteFavoriteFail = (error: any) => {
  return {
    type: actionTypes.DELETE_FAVORITE_FAIL,
    error: error,
  };
};

export const initValuesFavorite = () => {
  return {
    type: actionTypes.INIT_VALUES_FAVORITE,
  };
};

export const createFavorite = (favorite: any) => {
  return (dispatch: any) => {
    dispatch(createFavoriteStart());
    
    axios
      .post("api/favorites", favorite)
      .then((response) => {
        dispatch(createFavoriteSuccess());
      })
      .catch((error) => {
        dispatch(createFavoriteFail(error));
      });
  };
};

export const fetchFavorite = (storeId: string, userId: string) => {
  return (dispatch: any) => {
    dispatch(fetchFavoriteStart());
    
    axios
      .get("api/favorites/IsFavorite?storeId=" + storeId + "&userId=" + userId)
      .then((response) => {
        dispatch(fetchFavoriteSuccess(response.data as boolean));
      })
      .catch((error) => {
        dispatch(fetchFavoriteFail(error));
      });
  };
};

export const deleteFavorite = (favoriteId: string) => {
  return (dispatch: any) => {
    dispatch(deleteFavoriteStart());

    axios
      .delete("api/favorites/" + favoriteId)
      .then(() => {
        dispatch(deleteFavoriteSuccess());
        dispatch(removeFavorite(favoriteId));
      })
      .catch((error) => {
        dispatch(deleteFavoriteFail(error));
      });
  };
};

export const clearFavorite = () => {
  return (dispatch: any) => {
    dispatch(initValuesFavorite());
  };
};
