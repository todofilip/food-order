import axios from "../../shared/axios";
import * as actionTypes from "./actionTypes";

export const createProductStart = () => {
  return {
    type: actionTypes.CREATE_PRODUCT_START,
  };
};

export const createProductSuccess = (product: any) => {
  return {
    type: actionTypes.CREATE_PRODUCT_SUCCESS,
    product: product,
  };
};

export const createProductFail = (error: any) => {
  return {
    type: actionTypes.CREATE_PRODUCT_FAIL,
    error: error,
  };
};

export const updateProductStart = () => {
  return {
    type: actionTypes.UPDATE_PRODUCT_START,
  };
};

export const updateProductSuccess = (product: any) => {
  return {
    type: actionTypes.UPDATE_PRODUCT_SUCCESS,
    product: product,
  };
};

export const updateProductFail = (error: any) => {
  return {
    type: actionTypes.UPDATE_PRODUCT_FAIL,
    error: error,
  };
};

export const deleteProductStart = () => {
  return {
    type: actionTypes.DELETE_PRODUCT_START,
  };
};

export const deleteProductSuccess = () => {
  return {
    type: actionTypes.DELETE_PRODUCT_SUCCESS,
  };
};

export const deleteProductFail = (error: any) => {
  return {
    type: actionTypes.DELETE_PRODUCT_FAIL,
    error: error,
  };
};

export const createProduct = (product: any) => {
  return (dispatch: any) => {
    dispatch(createProductStart());

    axios
      .post("api/products", product)
      .then((response) => {
        dispatch(createProductSuccess(response.data));
      })
      .catch((error) => {
        dispatch(createProductFail(error));
      });
  };
};

export const fetchProductStart = () => {
  return {
    type: actionTypes.FETCH_PRODUCT_START,
  };
};

export const fetchProductSuccess = (product: any) => {
  return {
    type: actionTypes.FETCH_PRODUCT_SUCCESS,
    product: product,
  };
};

export const fetchProductFail = (error: any) => {
  return {
    type: actionTypes.FETCH_PRODUCT_FAIL,
    error: error,
  };
};

export const fetchProduct = (productId: string) => {
  return async (dispatch: any) => {
    dispatch(fetchProductStart());

    axios
      .get("api/products/" + productId)
      .then((response) => {
        dispatch(fetchProductSuccess(response.data));
      })
      .catch((error) => {
        dispatch(fetchProductFail(error));
      });
  };
};

export const updateProduct = (product: any) => {
  return (dispatch: any) => {
    dispatch(updateProductStart());

    axios
      .put("api/products/" + product.id, product)
      .then((response) => {
        dispatch(updateProductSuccess(response.data));
      })
      .catch((error) => {
        dispatch(updateProductFail(error));
      });
  };
};

export const deleteProduct = (productId: string) => {
  return (dispatch: any) => {
    dispatch(deleteProductStart());

    axios
      .delete("api/products/" + productId)
      .then(() => {
        dispatch(deleteProductSuccess());
      })
      .catch((error) => {
        dispatch(deleteProductFail(error));
      });
  };
};

export const clearProduct = () => {
  return (dispatch: any) => {
    dispatch(fetchProductSuccess(null));
  };
};
