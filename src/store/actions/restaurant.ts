import axios from "../../shared/axios";
import * as actionTypes from "./actionTypes";

export const fetchRestaurantsStart = () => {
  return {
    type: actionTypes.FETCH_RESTAURANTS_START,
  };
};

export const fetchRestaurantsSuccess = (restaurants: any) => {
  return {
    type: actionTypes.FETCH_RESTAURANTS_SUCCESS,
    restaurants: restaurants,
  };
};

export const fetchRestaurantsFail = (error: any) => {
  return {
    type: actionTypes.FETCH_RESTAURANTS_FAIL,
    error: error,
  };
};

export const showFilterChange = (showFilter: boolean) => {
  return {
    type: actionTypes.SHOW_FILTER_CHANGE,
    showFilter: showFilter,
  };
};

export const setInitValuesRestaurants = () => {
  return {
    type: actionTypes.SET_INIT_VALUES_RESTAURANTS,
  };
};

export const fetchRestaurants = (category: string) => {
  return async (dispatch: any) => {
    dispatch(fetchRestaurantsStart());

    axios
      .get("api/stores/GetAll?category=" + category)
      .then((response) => {
        dispatch(fetchRestaurantsSuccess(response.data));
      })
      .catch((error) => {
        dispatch(fetchRestaurantsFail(error));
      });
  };
};

export const clearRestaurants = () => {
  return (dispatch: any) => {
    dispatch(setInitValuesRestaurants());
  };
};
