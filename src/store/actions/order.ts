import axios from "../../shared/axios";
import * as actionTypes from "./actionTypes";

export const fetchOrdersStart = () => {
  return {
    type: actionTypes.FETCH_ORDERS_START,
  };
};

export const fetchOrdersSuccess = (orders: any) => {
  return {
    type: actionTypes.FETCH_ORDERS_SUCCESS,
    orders: orders,
  };
};

export const fetchOrdersFail = (error: any) => {
  return {
    type: actionTypes.FETCH_ORDERS_FAIL,
    error: error,
  };
};

export const fetchOrders = (userId: string | null, token: string) => {
  return async (dispatch: any) => {
    dispatch(fetchOrdersStart());

    axios
      .get(userId ? "api/orders/GetByUserId/" + userId : "api/orders/")
      .then((response: any) => {
        dispatch(fetchOrdersSuccess(response.data));
      })
      .catch((error) => {
        dispatch(fetchOrdersFail(error));
      });
  };
};

export const clearOrders = () => {
  return (dispatch: any) => {
    dispatch(fetchOrdersSuccess(null));
  };
};

export const updateOrderStatusStart = () => {
  return {
    type: actionTypes.UPDATE_ORDERSTATUS_START,
  };
};

export const updateOrderStatusSuccess = (order: any) => {
  return {
    type: actionTypes.UPDATE_ORDERSTATUS_SUCCESS,
    order: order,
  };
};

export const updateOrderStatusFail = (error: any) => {
  return {
    type: actionTypes.UPDATE_ORDERSTATUS_FAIL,
    error: error,
  };
};

export const updateOrderStatus = (orderId: string) => {
  return (dispatch: any) => {
    dispatch(updateOrderStatusStart());

    const order: any = {
      id: orderId,
      status: "delivered",
    };

    axios
      .patch("api/orders/" + orderId, order)
      .then((response) => {
        dispatch(updateOrderStatusSuccess(response.data as any));
      })
      .catch((error: any) => {
        dispatch(updateOrderStatusFail(error));
      });
  };
};
