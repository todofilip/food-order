import axios from "../../shared/axios";
import * as actionTypes from "./actionTypes";

export const createStoreStart = () => {
  return {
    type: actionTypes.CREATE_STORE_START,
  };
};

export const createStoreSuccess = (store: any) => {
  return {
    type: actionTypes.CREATE_STORE_SUCCESS,
    store: store,
  };
};

export const createStoreFail = (error: any) => {
  return {
    type: actionTypes.CREATE_STORE_FAIL,
    error: error,
  };
};

export const updateStoreStart = () => {
  return {
    type: actionTypes.UPDATE_STORE_START,
  };
};

export const updateStoreSuccess = (store: any) => {
  return {
    type: actionTypes.UPDATE_STORE_SUCCESS,
    store: store,
  };
};

export const updateStoreFail = (error: any) => {
  return {
    type: actionTypes.UPDATE_STORE_FAIL,
    error: error,
  };
};

export const deleteStoreStart = () => {
  return {
    type: actionTypes.DELETE_STORE_START,
  };
};

export const deleteStoreSuccess = () => {
  return {
    type: actionTypes.DELETE_STORE_SUCCESS,
  };
};

export const deleteStoreFail = (error: any) => {
  return {
    type: actionTypes.DELETE_STORE_FAIL,
    error: error,
  };
};

export const fetchStoreStart = () => {
  return {
    type: actionTypes.FETCH_STORE_START,
  };
};

export const fetchStoreSuccess = (store: any) => {
  return {
    type: actionTypes.FETCH_STORE_SUCCESS,
    store: store,
  };
};

export const fetchStoreFail = (error: any) => {
  return {
    type: actionTypes.FETCH_STORE_FAIL,
    error: error,
  };
};

export const createStore = (store: any) => {
  return (dispatch: any) => {
    dispatch(createStoreStart());

    axios
      .post("api/stores", store)
      .then((response) => {
        dispatch(createStoreSuccess(response.data));
      })
      .catch((error) => {
        dispatch(createStoreFail(error));
      });
  };
};

export const updateStore = (store: any) => {
  return (dispatch: any) => {
    dispatch(updateStoreStart());

    axios
      .put("api/stores/" + store.id, store)
      .then((response) => {
        dispatch(updateStoreSuccess(response.data));
      })
      .catch((error) => {
        dispatch(updateStoreFail(error));
      });
  };
};

export const deleteStore = (storeId: string) => {
  return (dispatch: any) => {
    dispatch(deleteStoreStart());

    axios
      .delete("api/stores/" + storeId)
      .then(() => {
        dispatch(deleteStoreSuccess());
      })
      .catch((error) => {
        dispatch(deleteStoreFail(error));
      });
  };
};

export const fetchStore = (storeId: string) => {
  return (dispatch: any) => {
    dispatch(fetchStoreStart());

    axios
      .get("api/stores/" + storeId)
      .then((response) => {
        dispatch(fetchStoreSuccess(response.data));
      })
      .catch((error) => {
        dispatch(fetchStoreFail(error));
      });
  };
};

export const clearStore = () => {
  return (dispatch: any) => {
    dispatch(fetchStoreSuccess(null));
  };
};
