import axios from "../../shared/axios";
import * as actionTypes from "./actionTypes";
import { emptyCart } from "./cart";

export const authStart = () => {
  return {
    type: actionTypes.AUTH_START,
  };
};

export const authSuccess = (
  token: string,
  userId: string,
  username: string,
  useDarkTheme: boolean
) => {
  return {
    type: actionTypes.AUTH_SUCCESS,
    token: token,
    userId: userId,
    username: username,
    useDarkTheme: useDarkTheme
  };
};

export const authFail = (error: string | null) => {
  return {
    type: actionTypes.AUTH_FAIL,
    error: error,
  };
};

export const logout = () => {
  localStorage.removeItem("token");
  localStorage.removeItem("expirationDate");
  localStorage.removeItem("userId");
  localStorage.removeItem("username");
  localStorage.removeItem("useDarkTheme");
  localStorage.removeItem("productsInCart");

  return {
    type: actionTypes.AUTH_LOGOUT,
  };
};

export const checkAuthTimeout = (expirationTime: number) => {
  return (dispatch: any) => {
    setTimeout(() => {
      dispatch(logout());
    }, expirationTime * 1000);
  };
};

export const auth = (username: string, password: string) => {
  return (dispatch: any) => {
    dispatch(authStart());

    const authData = {
      username: username,
      password: password,
    };
    
    axios
      .post("api/users/authenticate", authData)
      .then((response) => {
        const expirationDate = new Date(new Date().getTime() + 1 * 3600 * 1000);

        localStorage.setItem("token", response.data.token);
        localStorage.setItem("expirationDate", expirationDate.toString());
        localStorage.setItem("userId", response.data.userId);
        localStorage.setItem("username", response.data.username);
        localStorage.setItem("useDarkTheme", response.data.useDarkTheme);

        dispatch(
          authSuccess(
            response.data.token,
            response.data.userId,
            response.data.username,
            response.data.useDarkTheme
          )
        );
        dispatch(checkAuthTimeout(3600));
      })
      .catch((error) => {
        dispatch(authFail(error.message));
      });
  };
};

export const setAuthRedirectPath = (path: string) => {
  return {
    type: actionTypes.SET_AUTH_REDIRECT_PATH,
    path: path,
  };
};

export const authCheckState = () => {
  return (dispatch: any) => {
    const token = localStorage.getItem("token");
    if (!token) {
      dispatch(logout());
    } else {
      const expirationDate = new Date(localStorage.getItem("expirationDate")!);
      if (expirationDate <= new Date()) {
        dispatch(emptyCart());
        dispatch(logout());
      } else {
        dispatch(
          authSuccess(
            token,
            localStorage.getItem("userId")!,
            localStorage.getItem("username")!,
            localStorage.getItem("useDarkTheme") === "true"
          )
        );
        dispatch(
          checkAuthTimeout(
            (expirationDate.getTime() - new Date().getTime()) / 1000
          )
        );
      }
    }
  };
};
