import axios from "../../shared/axios";
import * as actionTypes from "./actionTypes";

export const fetchFavoritesStart = () => {
  return {
    type: actionTypes.FETCH_FAVORITES_START,
  };
};

export const fetchFavoritesSuccess = (favorites: any) => {
  return {
    type: actionTypes.FETCH_FAVORITES_SUCCESS,
    favorites: favorites,
  };
};

export const fetchFavoritesFail = (error: any) => {
  return {
    type: actionTypes.FETCH_FAVORITES_FAIL,
    error: error,
  };
};

export const fetchFavorites = (userId: string) => {
  return async (dispatch: any) => {
    dispatch(fetchFavoritesStart());

    axios
      .get("api/favorites/GetByUserId/" + userId)
      .then((response) => {
        dispatch(fetchFavoritesSuccess(response.data as any));
      })
      .catch((error) => {
        dispatch(fetchFavoritesFail(error));
      });
  };
};

export const removeFavorite = (favoriteId: string) => {
  return {
    type: actionTypes.REMOVE_FAVORITE,
    favoriteId: favoriteId,
  };
};

export const clearFavorites = () => {
  return (dispatch: any) => {
    dispatch(fetchFavoritesSuccess(null));
  };
};
