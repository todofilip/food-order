import * as actionTypes from "../actions/actionTypes";
import { updateObject } from "../../shared/utility";

interface IFavoritesState {
  favorites: any | null;
  error: any;
  loading: boolean;
  requestSent: boolean;
}

const initialState: IFavoritesState = {
  favorites: null,
  error: null,
  loading: true,
  requestSent: false,
};

const fetchFavoritesStart = (state: IFavoritesState, action: any) => {
  return updateObject(state, { error: null, loading: true, requestSent: true });
};

const fetchFavoritesSuccess = (state: IFavoritesState, action: any) => {
  return updateObject(state, {
    favorites: action.favorites,
    error: null,
    loading: false,
    requestSent: false,
  });
};

const fetchFavoritesFail = (state: IFavoritesState, action: any) => {
  return updateObject(state, {
    error: action.error,
    loading: false,
    requestSent: false,
  });
};

const removeFavorite = (state: any, action: any) => {
  const index = state.favorites.findIndex(
    (favorites: any) => favorites.id === action.favoriteId
  );

  const favorites = [...state.favorites];
  favorites.splice(index, 1);

  return updateObject(state, {
    favorites: favorites,
  });
};

const reducer = (state = initialState, action: any) => {
  switch (action.type) {
    case actionTypes.FETCH_FAVORITES_START:
      return fetchFavoritesStart(state, action);
    case actionTypes.FETCH_FAVORITES_SUCCESS:
      return fetchFavoritesSuccess(state, action);
    case actionTypes.FETCH_FAVORITES_FAIL:
      return fetchFavoritesFail(state, action);
    case actionTypes.REMOVE_FAVORITE:
      return removeFavorite(state, action);
    default:
      return state;
  }
};

export default reducer;
