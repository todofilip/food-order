import * as actionTypes from "../actions/actionTypes";
import { updateObject } from "../../shared/utility";

interface IFavoriteState {
  isFavorite: boolean | null;
  requestSent: boolean;
  error: any;
  loading: boolean;
  addedSuccessful: boolean;
  deletedSuccessful: boolean;
}

const initialState: IFavoriteState = {
  isFavorite: null,
  requestSent: false,
  error: null,
  loading: true,
  addedSuccessful: false,
  deletedSuccessful: false,
};

const createFavoriteStart = (state: IFavoriteState, action: any) => {
  return updateObject(state, { error: null, loading: true, requestSent: true });
};

const createFavoriteSuccess = (state: IFavoriteState, action: any) => {
  return updateObject(state, {
    isFavorite: action.isFavorite,
    error: null,
    loading: false,
    requestSent: false,
    addedSuccessful: true,
  });
};

const createFavoriteFail = (state: IFavoriteState, action: any) => {
  return updateObject(state, {
    error: action.error,
    loading: false,
    requestSent: false,
  });
};

const fetchFavoriteStart = (state: IFavoriteState, action: any) => {
  return updateObject(state, { error: null, loading: true, requestSent: true });
};

const fetchFavoriteSuccess = (state: IFavoriteState, action: any) => {
  return updateObject(state, {
    isFavorite: action.isFavorite,
    error: null,
    loading: false,
    requestSent: false,
    addedSuccessful: false,
    deletedSuccessful: false,
  });
};

const fetchFavoriteFail = (state: IFavoriteState, action: any) => {
  return updateObject(state, {
    error: action.error,
    loading: false,
    requestSent: false,
  });
};

const deleteFavoriteStart = (state: IFavoriteState, action: any) => {
  return updateObject(state, {
    error: null,
    loading: true,
    requestSent: true,
    deletedSuccessful: false,
  });
};

const deleteFavoriteSuccess = (state: IFavoriteState, action: any) => {
  return updateObject(state, {
    isFavorite: false,
    error: null,
    loading: false,
    requestSent: false,
    deletedSuccessful: true,
  });
};

const deleteFavoriteFail = (state: IFavoriteState, action: any) => {
  return updateObject(state, {
    error: action.error,
    loading: false,
    requestSent: false,
  });
};

const initValuesFavorite = (state: IFavoriteState, action: any) => {
  return updateObject(state, {
    isFavorite: null,
    error: null,
    loading: true,
    requestSent: false,
    addedSuccessful: false,
    deletedSuccessful: false,
  });
};

const reducer = (state = initialState, action: any) => {
  switch (action.type) {
    case actionTypes.CREATE_FAVORITE_START:
      return createFavoriteStart(state, action);
    case actionTypes.CREATE_FAVORITE_SUCCESS:
      return createFavoriteSuccess(state, action);
    case actionTypes.CREATE_FAVORITE_FAIL:
      return createFavoriteFail(state, action);
    case actionTypes.FETCH_FAVORITE_START:
      return fetchFavoriteStart(state, action);
    case actionTypes.FETCH_FAVORITE_SUCCESS:
      return fetchFavoriteSuccess(state, action);
    case actionTypes.FETCH_FAVORITE_FAIL:
      return fetchFavoriteFail(state, action);
    case actionTypes.DELETE_FAVORITE_START:
      return deleteFavoriteStart(state, action);
    case actionTypes.DELETE_FAVORITE_SUCCESS:
      return deleteFavoriteSuccess(state, action);
    case actionTypes.DELETE_FAVORITE_FAIL:
      return deleteFavoriteFail(state, action);
    case actionTypes.INIT_VALUES_FAVORITE:
      return initValuesFavorite(state, action);
    default:
      return state;
  }
};

export default reducer;
