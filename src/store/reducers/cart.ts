import * as actionTypes from "../actions/actionTypes";
import { updateObject } from "../../shared/utility";

const initialState = {
  productsInCart: [],
  orderId: null,
  loading: false,
  error: null,
  posted: false,
};

const fetchOrderStart = (state: any, action: any) => {
  return updateObject(state, { error: null, loading: true });
};

const fetchOrderSuccess = (state: any, action: any) => {
  return updateObject(state, {
    orderId: action.orderId,
    error: null,
    loading: false,
  });
};

const fetchOrderFail = (state: any, action: any) => {
  return updateObject(state, {
    error: action.error,
    loading: false,
  });
};

const postOrderStart = (state: any, action: any) => {
  return updateObject(state, { error: null, loading: true, posted: false });
};

const postOrderSuccess = (state: any, action: any) => {
  return updateObject(state, {
    productsInCart: [],
    error: null,
    loading: false,
    posted: true,
  });
};

const postOrderFail = (state: any, action: any) => {
  return updateObject(state, {
    error: action.error,
    loading: false,
  });
};

const updateCart = (state: any, action: any) => {
  return updateObject(state, {
    productsInCart: [...action.productsInCart],
  });
};

const createNewCart = (state: any, action: any) => {
  return updateObject(state, {
    productsInCart: [],
    posted: false,
  });
};

const reducer = (state = initialState, action: any) => {
  switch (action.type) {
    case actionTypes.UPDATE_CART:
      return updateCart(state, action);
    case actionTypes.CREATE_NEW_CART:
      return createNewCart(state, action);
    case actionTypes.POST_ORDER_START:
      return postOrderStart(state, action);
    case actionTypes.POST_ORDER_SUCCESS:
      return postOrderSuccess(state, action);
    case actionTypes.POST_ORDER_FAIL:
      return postOrderFail(state, action);
    case actionTypes.FETCH_ORDER_START:
      return fetchOrderStart(state, action);
    case actionTypes.FETCH_ORDER_SUCCESS:
      return fetchOrderSuccess(state, action);
    case actionTypes.FETCH_ORDER_FAIL:
      return fetchOrderFail(state, action);
    default:
      return state;
  }
};

export default reducer;
