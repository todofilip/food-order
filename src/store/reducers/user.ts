import * as actionTypes from "../actions/actionTypes";
import { updateObject } from "../../shared/utility";

interface IUserState {
  user: {
    username: string;
    password: string;
    firstName: string;
    lastName: string;
    useDarkTheme: boolean;
  } | null;
  requestSent: boolean;
  error: any;
  loading: boolean;
  createdSuccessful: boolean;
}

const initialState: IUserState = {
  user: null,
  error: null,
  loading: false,
  requestSent: false,
  createdSuccessful: false,
};

const createUserStart = (state: IUserState, action: any) => {
  return updateObject(state, {
    error: null,
    loading: true,
    requestSent: true,
    createdSuccessful: false,
  });
};

const createUserSuccess = (state: IUserState, action: any) => {
  return updateObject(state, {
    user: action.user,
    error: null,
    loading: false,
    requestSent: false,
    createdSuccessful: true,
  });
};

const createUserFail = (state: IUserState, action: any) => {
  return updateObject(state, {
    error: action.error,
    loading: false,
    requestSent: false,
  });
};

const fetchUserStart = (state: IUserState, action: any) => {
  return updateObject(state, { error: null, loading: true, requestSent: true });
};

const fetchUserSuccess = (state: IUserState, action: any) => {
  return updateObject(state, {
    user: action.user,
    error: null,
    loading: false,
    requestSent: false,
    createdSuccessful: false,
  });
};

const fetchUserFail = (state: IUserState, action: any) => {
  return updateObject(state, {
    error: action.error,
    loading: false,
    requestSent: false,
  });
};

const updateUserStart = (state: IUserState, action: any) => {
  return updateObject(state, { error: null, loading: true, requestSent: true });
};

const updateUserSuccess = (state: IUserState, action: any) => {
  return updateObject(state, {
    user: action.user,
    error: null,
    loading: false,
    requestSent: false,
  });
};

const updateUserFail = (state: IUserState, action: any) => {
  return updateObject(state, {
    error: action.error,
    loading: false,
    requestSent: false,
  });
};

const reducer = (state = initialState, action: any) => {
  switch (action.type) {
    case actionTypes.CREATE_USER_START:
      return createUserStart(state, action);
    case actionTypes.CREATE_USER_SUCCESS:
      return createUserSuccess(state, action);
    case actionTypes.CREATE_USER_FAIL:
      return createUserFail(state, action);
    case actionTypes.FETCH_USER_START:
      return fetchUserStart(state, action);
    case actionTypes.FETCH_USER_SUCCESS:
      return fetchUserSuccess(state, action);
    case actionTypes.FETCH_USER_FAIL:
      return fetchUserFail(state, action);
    case actionTypes.UPDATE_USER_START:
      return updateUserStart(state, action);
    case actionTypes.UPDATE_USER_SUCCESS:
      return updateUserSuccess(state, action);
    case actionTypes.UPDATE_USER_FAIL:
      return updateUserFail(state, action);
    default:
      return state;
  }
};

export default reducer;
