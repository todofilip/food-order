import * as actionTypes from "../actions/actionTypes";
import { updateObject } from "../../shared/utility";

interface IAuthState {
  token: string | null;
  userId: string | null;
  username: string | null;
  useDarkTheme: boolean;
  error: any;
  loading: boolean;
  authRedirectPath: string;
}

const initialState: IAuthState = {
  token: null,
  userId: null,
  username: null,
  useDarkTheme: false,
  error: null,
  loading: false,
  authRedirectPath: "/",
};

const authStart = (state: IAuthState, action: any) => {
  return updateObject(state, { error: null, loading: true });
};

const authSuccess = (state: IAuthState, action: any) => {
  return updateObject(state, {
    token: action.token,
    userId: action.userId,
    username: action.username,
    useDarkTheme: action.useDarkTheme,
    error: null,
    loading: false,
  });
};

const authFail = (state: IAuthState, action: any) => {
  return updateObject(state, {
    error: action.error,
    loading: false,
  });
};

const authLogout = (state: IAuthState, action: any) => {
  return updateObject(state, { token: null, userId: null, username: null });
};

const setAuthRedirectPath = (state: IAuthState, action: any) => {
  return updateObject(state, { authRedirectPath: action.path });
};

const reducer = (state = initialState, action: any) => {
  switch (action.type) {
    case actionTypes.AUTH_START:
      return authStart(state, action);
    case actionTypes.AUTH_SUCCESS:
      return authSuccess(state, action);
    case actionTypes.AUTH_FAIL:
      return authFail(state, action);
    case actionTypes.AUTH_LOGOUT:
      return authLogout(state, action);
    case actionTypes.SET_AUTH_REDIRECT_PATH:
      return setAuthRedirectPath(state, action);
    default:
      return state;
  }
};

export default reducer;
