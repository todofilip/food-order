import * as actionTypes from "../actions/actionTypes";
import { updateObject } from "../../shared/utility";

interface IProductsState {
  products: any | null;
  error: any;
  loading: boolean;
  requestSent: boolean;
  showFilter: boolean;
}

const initialState: IProductsState = {
  products: null,
  error: null,
  loading: true,
  requestSent: false,
  showFilter: false,
};

const fetchProductsStart = (state: IProductsState, action: any) => {
  return updateObject(state, { error: null, loading: true, requestSent: true });
};

const fetchProductsSuccess = (state: IProductsState, action: any) => {
  return updateObject(state, {
    products: action.products,
    error: null,
    loading: false,
    requestSent: false,
  });
};

const fetchProductsFail = (state: IProductsState, action: any) => {
  return updateObject(state, {
    error: action.error,
    loading: false,
    requestSent: false,
  });
};

const showProductFilterChange = (state: IProductsState, action: any) => {
  return updateObject(state, {
    showFilter: action.showFilter,
  });
};

const reducer = (state = initialState, action: any) => {
  switch (action.type) {
    case actionTypes.FETCH_PRODUCTS_START:
      return fetchProductsStart(state, action);
    case actionTypes.FETCH_PRODUCTS_SUCCESS:
      return fetchProductsSuccess(state, action);
    case actionTypes.FETCH_PRODUCTS_FAIL:
      return fetchProductsFail(state, action);
    case actionTypes.SHOW_PRODUCT_FILTER_CHANGE:
      return showProductFilterChange(state, action);
    default:
      return state;
  }
};

export default reducer;
