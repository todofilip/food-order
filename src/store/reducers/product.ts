import * as actionTypes from "../actions/actionTypes";
import { updateObject } from "../../shared/utility";

interface IProductState {
  product: any;
  requestSent: boolean;
  error: any;
  loading: boolean;
  deletedSuccessful: boolean;
}

const initialState: IProductState = {
  product: null,
  requestSent: false,
  error: null,
  loading: true,
  deletedSuccessful: false,
};

const createProductStart = (state: IProductState, action: any) => {
  return updateObject(state, { error: null, loading: true, requestSent: true });
};

const createProductSuccess = (state: IProductState, action: any) => {
  return updateObject(state, {
    product: action.product,
    error: null,
    loading: false,
    requestSent: false,
  });
};

const createProductFail = (state: IProductState, action: any) => {
  return updateObject(state, {
    error: action.error,
    loading: false,
    requestSent: false,
  });
};

const fetchProductStart = (state: IProductState, action: any) => {
  return updateObject(state, { error: null, loading: true, requestSent: true });
};

const fetchProductSuccess = (state: IProductState, action: any) => {
  return updateObject(state, {
    product: action.product,
    error: null,
    loading: false,
    requestSent: false,
    deletedSuccessful: false,
  });
};

const fetchProductFail = (state: IProductState, action: any) => {
  return updateObject(state, {
    error: action.error,
    loading: false,
    requestSent: false,
  });
};

const updateProductStart = (state: IProductState, action: any) => {
  return updateObject(state, { error: null, loading: true, requestSent: true });
};

const updateProductSuccess = (state: IProductState, action: any) => {
  return updateObject(state, {
    product: action.product,
    error: null,
    loading: false,
    requestSent: false,
  });
};

const updateProductFail = (state: IProductState, action: any) => {
  return updateObject(state, {
    error: action.error,
    loading: false,
    requestSent: false,
  });
};

const deleteProductStart = (state: IProductState, action: any) => {
  return updateObject(state, {
    error: null,
    loading: true,
    requestSent: true,
    deletedSuccessful: false,
  });
};

const deleteProductSuccess = (state: IProductState, action: any) => {
  return updateObject(state, {
    product: null,
    error: null,
    loading: false,
    requestSent: false,
    deletedSuccessful: true,
  });
};

const deleteProductFail = (state: IProductState, action: any) => {
  return updateObject(state, {
    error: action.error,
    loading: false,
    requestSent: false,
  });
};

const reducer = (state = initialState, action: any) => {
  switch (action.type) {
    case actionTypes.CREATE_PRODUCT_START:
      return createProductStart(state, action);
    case actionTypes.CREATE_PRODUCT_SUCCESS:
      return createProductSuccess(state, action);
    case actionTypes.CREATE_PRODUCT_FAIL:
      return createProductFail(state, action);
    case actionTypes.FETCH_PRODUCT_START:
      return fetchProductStart(state, action);
    case actionTypes.FETCH_PRODUCT_SUCCESS:
      return fetchProductSuccess(state, action);
    case actionTypes.FETCH_PRODUCT_FAIL:
      return fetchProductFail(state, action);
    case actionTypes.UPDATE_PRODUCT_START:
      return updateProductStart(state, action);
    case actionTypes.UPDATE_PRODUCT_SUCCESS:
      return updateProductSuccess(state, action);
    case actionTypes.UPDATE_PRODUCT_FAIL:
      return updateProductFail(state, action);
    case actionTypes.DELETE_PRODUCT_START:
      return deleteProductStart(state, action);
    case actionTypes.DELETE_PRODUCT_SUCCESS:
      return deleteProductSuccess(state, action);
    case actionTypes.DELETE_PRODUCT_FAIL:
      return deleteProductFail(state, action);
    default:
      return state;
  }
};

export default reducer;
