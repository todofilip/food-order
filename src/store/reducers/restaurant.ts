import * as actionTypes from "../actions/actionTypes";
import { updateObject } from "../../shared/utility";

interface IRestaurantState {
  restaurants: any | null;
  error: any;
  loading: boolean;
  requestSent: boolean;
  showFilter: boolean;
}

const initialState: IRestaurantState = {
  restaurants: null,
  error: null,
  loading: true,
  requestSent: false,
  showFilter: false,
};

const fetchRestaurantsStart = (state: IRestaurantState, action: any) => {
  return updateObject(state, { error: null, loading: true, requestSent: true });
};

const fetchRestaurantsSuccess = (state: IRestaurantState, action: any) => {
  return updateObject(state, {
    restaurants: action.restaurants,
    error: null,
    loading: false,
    requestSent: false,
  });
};

const fetchRestaurantsFail = (state: IRestaurantState, action: any) => {
  return updateObject(state, {
    error: action.error,
    loading: false,
    requestSent: false,
  });
};

const showFilterChange = (state: IRestaurantState, action: any) => {
  return updateObject(state, {
    showFilter: action.showFilter,
  });
};

const setInitValuesRestaurants = (state: IRestaurantState, action: any) => {
  return updateObject(state, {
    restaurants: null,
    error: null,
    loading: true,
    requestSent: false,
    showFilter: false,
  });
};

const reducer = (state = initialState, action: any) => {
  switch (action.type) {
    case actionTypes.FETCH_RESTAURANTS_START:
      return fetchRestaurantsStart(state, action);
    case actionTypes.FETCH_RESTAURANTS_SUCCESS:
      return fetchRestaurantsSuccess(state, action);
    case actionTypes.FETCH_RESTAURANTS_FAIL:
      return fetchRestaurantsFail(state, action);
    case actionTypes.SHOW_FILTER_CHANGE:
      return showFilterChange(state, action);
    case actionTypes.SET_INIT_VALUES_RESTAURANTS:
      return setInitValuesRestaurants(state, action);
    default:
      return state;
  }
};

export default reducer;
