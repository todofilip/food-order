import * as actionTypes from "../actions/actionTypes";
import { updateObject } from "../../shared/utility";

interface IOrderState {
  orders: any | null;
  error: any;
  loading: boolean;
  requestSent: boolean;
}

const initialState: IOrderState = {
  orders: null,
  error: null,
  loading: true,
  requestSent: false,
};

const fetchOrdersStart = (state: IOrderState, action: any) => {
  return updateObject(state, { error: null, loading: true, requestSent: true });
};

const fetchOrdersSuccess = (state: IOrderState, action: any) => {
  return updateObject(state, {
    orders: action.orders,
    error: null,
    loading: false,
    requestSent: false,
  });
};

const fetchOrdersFail = (state: IOrderState, action: any) => {
  return updateObject(state, {
    error: action.error,
    loading: false,
    requestSent: false,
  });
};

const updateOrderStatusStart = (state: any, action: any) => {
  return updateObject(state, { error: null, loading: true });
};

const updateOrderStatusSuccess = (state: any, action: any) => {
  const index = state.orders.findIndex(
    (orders: any) => orders.id === action.order.id
  );

  const orders = [...state.orders];
  const order = { ...orders[index] };
  order.status = action.order.status;
  orders[index] = order;

  return updateObject(state, {
    orders: orders,
  });
};

const updateOrderStatusFail = (state: any, action: any) => {
  return updateObject(state, {
    error: action.error,
    loading: false,
  });
};

const reducer = (state = initialState, action: any) => {
  switch (action.type) {
    case actionTypes.FETCH_ORDERS_START:
      return fetchOrdersStart(state, action);
    case actionTypes.FETCH_ORDERS_SUCCESS:
      return fetchOrdersSuccess(state, action);
    case actionTypes.FETCH_ORDERS_FAIL:
      return fetchOrdersFail(state, action);
    case actionTypes.UPDATE_ORDERSTATUS_START:
      return updateOrderStatusStart(state, action);
    case actionTypes.UPDATE_ORDERSTATUS_SUCCESS:
      return updateOrderStatusSuccess(state, action);
    case actionTypes.UPDATE_ORDERSTATUS_FAIL:
      return updateOrderStatusFail(state, action);
    default:
      return state;
  }
};

export default reducer;
