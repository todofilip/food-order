import * as actionTypes from "../actions/actionTypes";
import { updateObject } from "../../shared/utility";

interface IStoreState {
  store: any;
  requestSent: boolean;
  error: any;
  loading: boolean;
  deletedSuccessful: boolean;
}

const initialState: IStoreState = {
  store: null,
  requestSent: false,
  error: null,
  loading: false,
  deletedSuccessful: false,
};

const createStoreStart = (state: IStoreState, action: any) => {
  return updateObject(state, { error: null, loading: true, requestSent: true });
};

const createStoreSuccess = (state: IStoreState, action: any) => {
  return updateObject(state, {
    store: action.store,
    error: null,
    loading: false,
    requestSent: false,
  });
};

const createStoreFail = (state: IStoreState, action: any) => {
  return updateObject(state, {
    error: action.error,
    loading: false,
    requestSent: false,
  });
};

const deleteStoreStart = (state: IStoreState, action: any) => {
  return updateObject(state, {
    error: null,
    loading: true,
    requestSent: true,
    deletedSuccessful: false,
  });
};

const deleteStoreSuccess = (state: IStoreState, action: any) => {
  return updateObject(state, {
    store: null,
    error: null,
    loading: false,
    requestSent: false,
    deletedSuccessful: true,
  });
};

const deleteStoreFail = (state: IStoreState, action: any) => {
  return updateObject(state, {
    error: action.error,
    loading: false,
    requestSent: false,
  });
};

const updateStoreStart = (state: IStoreState, action: any) => {
  return updateObject(state, { error: null, loading: true, requestSent: true });
};

const updateStoreSuccess = (state: IStoreState, action: any) => {
  return updateObject(state, {
    store: action.store,
    error: null,
    loading: false,
    requestSent: false,
  });
};

const updateStoreFail = (state: IStoreState, action: any) => {
  return updateObject(state, {
    error: action.error,
    loading: false,
    requestSent: false,
  });
};

const fetchStoreStart = (state: IStoreState, action: any) => {
  return updateObject(state, { error: null, loading: true, requestSent: true });
};

const fetchStoreSuccess = (state: IStoreState, action: any) => {
  return updateObject(state, {
    store: action.store,
    error: null,
    loading: false,
    requestSent: false,
    deletedSuccessful: false,
  });
};

const fetchStoreFail = (state: IStoreState, action: any) => {
  return updateObject(state, {
    error: action.error,
    loading: false,
    requestSent: false,
  });
};

const reducer = (state = initialState, action: any) => {
  switch (action.type) {
    case actionTypes.CREATE_STORE_START:
      return createStoreStart(state, action);
    case actionTypes.CREATE_STORE_SUCCESS:
      return createStoreSuccess(state, action);
    case actionTypes.CREATE_STORE_FAIL:
      return createStoreFail(state, action);
    case actionTypes.UPDATE_STORE_START:
      return updateStoreStart(state, action);
    case actionTypes.UPDATE_STORE_SUCCESS:
      return updateStoreSuccess(state, action);
    case actionTypes.UPDATE_STORE_FAIL:
      return updateStoreFail(state, action);
    case actionTypes.DELETE_STORE_START:
      return deleteStoreStart(state, action);
    case actionTypes.DELETE_STORE_SUCCESS:
      return deleteStoreSuccess(state, action);
    case actionTypes.DELETE_STORE_FAIL:
      return deleteStoreFail(state, action);
    case actionTypes.FETCH_STORE_START:
      return fetchStoreStart(state, action);
    case actionTypes.FETCH_STORE_SUCCESS:
      return fetchStoreSuccess(state, action);
    case actionTypes.FETCH_STORE_FAIL:
      return fetchStoreFail(state, action);
    default:
      return state;
  }
};

export default reducer;
