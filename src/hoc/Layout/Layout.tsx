import React from "react";
import Header from "../../components/Header/Header";
import {
  CssBaseline,
  makeStyles,
  Theme,
  createStyles,
} from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
      minHeight: '100%',
      
      backgroundColor: theme.palette.background.default
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
    },
    content: {
      flexGrow: 1,
      marginTop: theme.spacing(8),
      // marginBottom: theme.spacing(6),
    },
  })
);

interface LayoutProps {
  isAuthenticated: boolean;
  username: string;
}

const Layout: React.SFC<LayoutProps> = (props: any) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Header isAuthenticated={props.isAuthenticated} username={props.username} />
      <main className={classes.content}>{props.children}</main>
      {/* <Footer /> */}
    </div>
  );
};

export default Layout;
