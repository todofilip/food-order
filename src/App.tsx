import React, { useEffect } from "react";
import { Route, Switch, Redirect, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { ThemeProvider, createMuiTheme } from "@material-ui/core";

import Login from "./components/Login/Login";
import Logout from "./components/Logout/Logout";
import Register from "./components/Register/Register";
import * as actions from "./store/actions/index";
import User from "./components/User/User";
import Layout from "./hoc/Layout/Layout";
import SearchRestaurants from "./components/SearchRestaurants/SearchRestaurants";
import SearchProducts from "./components/SearchProducts/SearchProducts";
import Success from "./components/Success/Success";
import Orders from "./components/Orders/Orders";
import Stores from "./components/Stores/Stores";
import CreateStore from "./components/Stores/Store/CreateStore";
import EditStore from "./components/Stores/Store/EditStore";
import CreateProduct from "./components/SearchProducts/Products/Product/CreateProduct";
import EditProduct from "./components/SearchProducts/Products/Product/EditProduct";
import ManageOrders from "./components/ManageOrders/ManageOrders";
import Favorites from "./components/Favorites/Favorites";
import Help from "./components/Help/Help";

interface AppProps {
  isAuthenticated: boolean;
  username: string;
  useDarkTheme: boolean | null;
  onTryAutoSignup(): void;
  onSyncProductsInCart(): void;
}

const App: React.SFC<AppProps> = (props) => {
  const { onTryAutoSignup } = props;
  const { onSyncProductsInCart } = props;
  const { username, useDarkTheme } = props;

  useEffect(() => {
    onTryAutoSignup();
    onSyncProductsInCart();
  }, [onTryAutoSignup]);

  let routes = (
    <Switch>
      <Route path="/login" component={Login} />
      <Route path="/register" component={Register} />
      <Redirect to="/login" />
    </Switch>
  );

  if (props.isAuthenticated) {
    routes = (
      <Switch>
        <Route path="/store/:id" component={EditStore} />
        <Route path="/store" component={CreateStore} />
        <Route path="/stores" component={Stores} />
        <Route path="/product/:id" component={EditProduct} />
        <Route path="/product" component={CreateProduct} />
        <Route path="/profile" component={User} />
        <Route path="/logout" component={Logout} />
        <Route path="/success/:id" component={Success} />
        <Route path="/orders" component={Orders} />
        <Route path="/manageOrders" component={ManageOrders} />
        <Route path="/restaurant/:id" component={SearchProducts} />
        <Route path="/favorites" component={Favorites} />
        <Route path="/help" component={Help} />
        <Route
          path="/"
          exact
          component={username === "admin" ? Stores : SearchRestaurants}
        />
        <Redirect to="/" />
      </Switch>
    );
  }

  const paletteDark: any = {
    type: "dark",
    primary: {
      main: "#ab003c",
    },
    secondary: {
      main: "#f50057",
    },
    background: {
      default: "#333333"
    },
  };

  const paletteLight: any = {
    type: "light",
    primary: {
      main: "#ab003c",
    },
    secondary: {
      main: "#f50057",
    },
    background: {
      default: "#eee",
    },
  };

  const theme = createMuiTheme({
    palette: useDarkTheme ? paletteDark : paletteLight,
  });

  return (
    <ThemeProvider theme={theme}>
      <Layout isAuthenticated={props.isAuthenticated} username={props.username}>
        {routes}
      </Layout>
    </ThemeProvider>
  );
};

const mapStateToProps = (state: any) => {
  return {
    isAuthenticated: state.auth.token !== null,
    username: state.auth.username,
    useDarkTheme: state.auth.useDarkTheme,
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    onTryAutoSignup: () => dispatch(actions.authCheckState()),
    onSyncProductsInCart: () => dispatch(actions.syncProductsInCart()),
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
